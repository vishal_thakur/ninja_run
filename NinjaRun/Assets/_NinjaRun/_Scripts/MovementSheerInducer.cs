using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSheerInducer : MonoBehaviour
{
    // Start is called before the first frame update

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Shear"+ other.gameObject.GetComponent<CharacterMovement>().shear);
            if (!other.gameObject.GetComponent<CharacterMovement>().shear)
            {
                other.gameObject.GetComponent<CharacterMovement>().StartShear();
            }
            else
            {
                other.gameObject.GetComponent<CharacterMovement>().StopShear();
            }
            //other.gameObject.GetComponent<CharacterMovement>().StartShear();\
        }
    }



    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.gameObject.tag.Equals("Player"))
    //    {
    //        Debug.Log("Shear");
    //        collision.gameObject.GetComponent<CharacterMovement>().StartShear();
    //    }
    //}
}
