using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LaserLightMove : MonoBehaviour
{
    // Start is called before the first frame update
    public Renderer lightRenderer;
    public List<Color> lightColors;
    void Start()
    {
        int rand = Random.Range(0, lightColors.Count);
        StartCoroutine(setLightsAndStartRotate(rand));
    }

    IEnumerator setLightsAndStartRotate(int delay)
    {
        yield return new WaitForSeconds(1);
        lightRenderer.material.SetColor("_EmissionColor", lightColors[delay]);
        lightRenderer.material.SetColor("_BaseColor", lightColors[delay]);
        lightRenderer.material.SetVector("_EmissionColor", lightColors[delay] * 3f);
       //transform.DORotate(new Vector3(100f, 0, 0), 4f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
       transform.DORotateQuaternion(Quaternion.identity, 2f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
