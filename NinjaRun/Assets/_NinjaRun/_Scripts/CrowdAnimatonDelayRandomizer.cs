using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdAnimatonDelayRandomizer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int delay = Random.Range(0, 4);
        Invoke("StartAnimator", delay);
    }

    void StartAnimator()
    {
        GetComponent<Animator>().enabled = true;
    }

 
}
