﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public CharacterController controller;
    private Vector3 playerVelocity;
    public bool groundedPlayer;
    private float playerSpeed = 5.0f;
    private float jumpHeight = 3f;
    private float gravityValue = -2f;
    public bool canMove = false;
    public bool hasjumped, salmonLadderCompleted;
    Rigidbody rb;

    public float jumpForce,forwardForce;
    public float pushForce;
    public bool fastTapMiniGame;
    public Transform rampStartPos;
    public Transform rampClimbPos;
    public Transform climbingDonePosition;
    public Transform respawnPoint;
    public Transform[] salmonclimbPos;

    //
    public GameObject playerHand;
    public GameObject lHand;
    public GameObject rHand;
    public List<GameObject> winPanelList;
    public GameObject pipeEndObj;
    public Transform ladderSpawnPoint;
    public bool isPipeComplete;
    public bool isLadderComplete;
    Animator animator;
    bool isEnableClimbed;

    [SerializeField]
    GameObject missPanel;

    public float currentVel;
    private void Start()
    {
        //controller = gameObject.AddComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        isEnableClimbed = false;
    }

    public void ShowMissPanel()
    {
        missPanel.gameObject.SetActive(true);
    }
    void Update()
    {

        if (!canMove)
        {
            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    Jump();
            //}
            if (fastTapMiniGame)
            {
                rb.AddForce(Vector3.forward * deaccSpeed * Time.deltaTime);
            }
            currentVel = rb.velocity.magnitude;
            return;
        }
        //groundedPlayer = controller.isGrounded;
        ////if (groundedPlayer && playerVelocity.y < 0)
        ////{
        ////    playerVelocity.y = 0f;
        ////}
        //
        //if (fastTapMiniGame)
        //{
        //    moveSpeed = Mathf.Clamp(moveSpeed - deaccSpeed * Time.deltaTime, 0, storeMoveSpeed);
        //}
        float forwardSpeed = (canMove ? moveSpeed : 0);
        if (!shear)
        {
            shearValue = 0;
        }

        Vector3 move = new Vector3(shearValue, 0, forwardSpeed * playerSpeed);
        //controller.Move(move * Time.deltaTime * playerSpeed);
        //
        //if (move != Vector3.zero)
        //{
        //    gameObject.transform.forward = move;
        //}
        //
        //// Changes the height position of the player..
        //if (hasjumped )//&& groundedPlayer)
        //{
        //    playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        //    hasjumped = false;
        //}
        //
        //playerVelocity.y += gravityValue * Time.deltaTime;
        //controller.Move(playerVelocity * Time.deltaTime);
        transform.Translate(move * Time.deltaTime);

        //Jump
        
    }
    public void StopMovement()
    {
        canMove = false;
        animator.SetBool("Run", canMove);
        animator.SetTrigger("Warmup");

    }

    public void Run()
    {
        canMove = true;
        animator.SetBool("Run", canMove);
    }

    public GameObject perfectUIPanel;
    public void ShowPerfectUI()
    {
        perfectUIPanel.SetActive(true);
    }
    public void MoveCharacterY(int id)
    {
        if(id == 1)
        {
            //hasjumped = true;
            //controller.enabled = false;
        }
        else
        {
            //hasjumped = false;
            
        }

        Debug.Log("Called From AnimationY");
    }


    public void SetPush()
    {
        rb.AddForce(Vector3.forward * pushForce * Time.deltaTime, ForceMode.VelocityChange);
        animator.SetBool("Run", true);
    }

    public float deaccSpeed;
    public float storeMoveSpeed;
    public void DecreaseSpeed()
    {
        fastTapMiniGame = true;
        canTakeFastTap = true;
    }

    public bool canTakeFastTap;
    public void IncreaseSpeed()
    {
        if(!canTakeFastTap)
        {
            return;
        }
        rb.AddForce(Vector3.forward * pushForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        animator.SetBool("Run", true);
    }

    public GameObject FastTapMiniGamePanel;
    public void EnableClimb()
    {
        if(!isEnableClimbed)
        {
            isEnableClimbed = true;
            Debug.Log("enable climb");
            fastTapMiniGame = false;
            canTakeFastTap = false;
            animator.SetBool("slideback", false);
            rb.isKinematic = true;
            animator.SetBool("climb", true);
            //CameraMovement.instance.followPlayer = false;
            //CameraMovement.instance.EnableCameraPositionEnd();
            perfectUIPanel.SetActive(true);
            FastTapMiniGamePanel.SetActive(false);
        }
       
    }

    

    public Collider animCollider;
    public void SetFallAnim()
    {
        if(!fastTapMiniGame)
        {
            return;
        }
        animator.SetBool("slideback", true);
        animator.SetBool("Run", false);
        canTakeFastTap = false;
        animCollider.enabled = true;
        StartCoroutine(CheckPlayerStop());
    }

    IEnumerator CheckPlayerStop()
    {
        while(currentVel>0.01f)
        {
            yield return null;
        }
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("Running"))
        {
            //Debug.Log(state.IsName(curState) + "state" + state.IsName(prevState));
            yield return null;

        }
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("DyingBackwards") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.8f)
        {
            //Debug.Log(state.IsName(curState) + "state" + state.IsName(prevState));
            yield return null;

        }
        Debug.Log("player stopped");
        if(!canTakeFastTap)
        {
            ResetFallAnim(true);
        }
    }
    public void ResetFallAnim(bool setPos = false)
    {
        Debug.Log("cantake" + canTakeFastTap);
       if(setPos) transform.position = rampStartPos.position;
        animator.SetBool("slideback", false);
        canTakeFastTap = true;
        animCollider.enabled = false;
    }

   
    public void RampEventPos()
    {
        transform.position = rampClimbPos.position;
    }

   
    public void RampClimbDone()
    {
        animator.SetBool("Run", true);
        animator.SetBool("climb", false);

        transform.position = GameObject.Find("climbDone").transform.position;
        GetComponent<Rigidbody>().isKinematic = false;
        canMove = true;
        
    }

    public void MoveCharacterZ()
    {
       
        //controller.enabled = true;
        Debug.Log("Called From AnimationZ");
        //controller.enabled = true;
        //controller.Move(Vector3.forward * 4f);
        animator.SetBool("Swing", false);
        animator.SetBool("Run", true);
        transform.position += new Vector3(0, 0, 5);
        canMove = true;
        //transform.position += Vector3.forward * 3;
        //transform.position = transform.position + Vector3.forward * 3;
    }


    public float moveSpeed;
    public bool shear;
    float shearValue;
    public float tempMoveSpeed;
    public void SetShearValue(float val)
    {
        shearValue = val;
    }

    public void StopShear()
    {
        isPipeComplete = true;
        pipeEndObj.SetActive(false);
        shear = false;
        animator.SetBool("Shear", false);
        GetComponent<SwipeMiniGameScript>().enabled = false;
        moveSpeed = 1.5f;
        ShowPerfectUI();
        transform.position = new Vector3(0, transform.position.y, transform.position.z);
    }

    public void StartShear()
    {
        shear = true;
        float val = Random.Range(-2,2);
        if(val > 0)
        {
            val = 0.5f;
        }
        else
        {
            val = -0.5f;
        }
        SetShearValue(val);
        animator.SetBool("Shear", true);
        GetComponent<SwipeMiniGameScript>().enabled = true;

    }
   

   
    public void SetDeath()
    {
        missPanel.SetActive(true);
        print("death");
        //StopShear();
        CameraMovement.instance.ResetCamParentPos();

        //   if(Globals.LoadedLevel == 0)
        if (Globals.CurrentMiniGame == Globals.miniGame.salmon)
        {
            salmonclimbPos[0].GetComponent<enableHanging>().entered = false;
            Bar.instance.positiontoOg();
        }

        animator.SetBool("Run", false);
        animator.SetBool("Shear", false);

        // if (Globals.LoadedLevel == 0)
        if (Globals.CurrentMiniGame != Globals.miniGame.rockClimb)
        {
            animator.SetTrigger("death");
        }

        rb.isKinematic = true;
        canMove = false;
        shear = false;
        animator.SetBool("Shear", false);
        GetComponent<SwipeMiniGameScript>().enabled = false;
        moveSpeed = 1.5f;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX;
        if(!isPipeComplete)
        {
            CameraMovement.instance.EnableCamMotionPipeDie();
        }
        else if(isPipeComplete)

        {
            CameraMovement.instance.EnableCamMotionLadderDie();
        }
       
        Invoke("ResetPlayer", 1.75f);
    }

    public void ResetPlayer()
    {
        CameraMovement.instance.ShowDefaultView();
        if (!isPipeComplete)
        {
            transform.position = new Vector3(0, 0, -5);
        }
        else if(isPipeComplete)
        {
            if (ladderSpawnPoint)
                transform.position = ladderSpawnPoint.position;
        }

        if (Bar.instance)
            Bar.instance.playerDead = false;

        if (Globals.CurrentMiniGame == Globals.miniGame.rockClimb)
            transform.position = GameObject.FindObjectOfType<RockClimbGameManager>().RespawnPoint.position;
        else if(Globals.CurrentMiniGame == Globals.miniGame.swingJump)
            transform.position = GameObject.FindObjectOfType<SwingGameManager>().RespawnPoint.position;

        transform.eulerAngles = Vector3.zero;
        CameraMovement.instance.lookatPlayer = false;
        animator.ResetTrigger("hung");
        animator.ResetTrigger("jumpToHang");
        animator.ResetTrigger("death");
        animator.SetBool("Run", true);
        rb.isKinematic = false ;
        canMove = true;
        EnableHandCollider();

        if (Globals.CurrentMiniGame == Globals.miniGame.pipe)
            pipeEndObj.SetActive(true);


        if (Globals.CurrentMiniGame == Globals.miniGame.rockClimb)
        {
            GameObject.FindObjectOfType<RockClimbGameManager>().ResetDefaults();

        }

        CameraMovement.instance.AllowFollow(true,false);
    }

    public void DoWin()
    {
        canMove = false;
        rb.isKinematic = true;
        rb.constraints = RigidbodyConstraints.None;
        transform.Rotate(0, 180, 0);
        animator.SetTrigger("win");
    }

    public void resetPos() { 
    }

    //salman

    public void Jump(bool isFail=false) {
        if(isFail)
        {
            rb.AddForce(0, jumpForce/1.5f, forwardForce/2, ForceMode.Impulse);
        }
        else
        rb.AddForce(0, jumpForce, forwardForce, ForceMode.Impulse);
    }
    public void EnableHandCollider()
    {
        lHand.GetComponent<Collider>().enabled = true;
        rHand.GetComponent<Collider>().enabled = true;
    }
    public void DisableHandCollider()
    {
        lHand.GetComponent<Collider>().enabled = false;
        rHand.GetComponent<Collider>().enabled = false;

    }

    public void EnableHanging()
    {
        perfectUIPanel.SetActive(true);

        //get collider
        lerpToPos(salmonclimbPos[0]);
        transform.position = salmonclimbPos[0].position;
        GetComponent<Rigidbody>().isKinematic = true;
        animator.SetTrigger("hung");
        checkedTheBarSupportNumber();
        
    }

    public void checkedTheBarSupportNumber()
    {
        if (Bar.instance.connectedToSupport && !salmonLadderCompleted)
        {

            StartCoroutine(ActivateNextWinview(0.9f,Bar.instance.supportNum));


        }
    }

    IEnumerator ActivateNextWinview(float sec,int supportNumber) {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        yield return new WaitForSeconds(sec);
        winPanelList[supportNumber].SetActive(true);
        //GameObject.Find("winPanels").transform.GetChild(supportNumber).gameObject.SetActive(true);

    }

    private void lerpToPos( Transform pos)
    {
        Vector3.Lerp(transform.position,pos.position,Time.deltaTime);
    }
}
