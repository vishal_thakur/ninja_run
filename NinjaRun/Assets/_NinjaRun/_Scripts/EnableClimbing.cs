using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Events;

public class EnableClimbing : MonoBehaviour
{

    [SerializeField]
    public UnityEvent methodToExecute;

    //[SerializeField]
    //bool ResetCameraToDefaultView = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

 

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            //if(ResetCameraToDefaultView)
            //    GameObject.FindObjectOfType<CameraMovement>().ShowDefaultView();

            methodToExecute.Invoke();

            // Debug.LogError("Gonna climb");
            other.gameObject.GetComponent<CharacterMovement>().EnableClimb();
            //GameObject.FindObjectOfType<CameraMovement>().followPlayer = true;
        }
    }
}
