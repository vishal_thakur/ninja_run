using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{
    public static Bar instance;
    public bool connectedToSupport,havePlayer;
    public int supportNum;
    GameObject Hand;
   [SerializeField] Transform originalParent;
    public MeshCollider[] supportBarCollider;
    Vector3 startPos;
    public GameObject player;
    public bool playerDead  =false; 
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        connectedToSupport = true;
        originalParent = gameObject.transform.parent;
        startPos = transform.position;
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (havePlayer && connectedToSupport)
    //    {
    //        //call another View
    //    }
    //} 
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "playerHand" && !playerDead)
        {
           // Debug.LogError("hand trigger");
            transform.parent = player.transform;
            //transform.parent = other.transform;
            Hand = other.gameObject;
            startPos = transform.position;

            //GetComponent<FixedJoint>().connectedBody = other.transform.root.GetComponent<Rigidbody>();
            //transform.position = Vector3.zero;

            other.transform.root.GetComponent<Rigidbody>().isKinematic = false ;
            havePlayer = true;
            //show winview 1
            

        }
    }
    public void MakeChild(GameObject obj)
    {
        transform.parent = obj.transform;
        Hand = obj;
    }


    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "ladderSupport")
        {
            connectedToSupport = true;

        }
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "ladderSupport")
        {
            connectedToSupport = false;
        }
        
    }

 


    public void GoToOriginalParent() {
        Debug.Log("go to original");
        transform.parent = originalParent;
        //GetComponent<FixedJoint>().connectedBody = null;
        //Hand.transform.root.GetComponent<Rigidbody>().isKinematic = true;

    }
    public void removeFromHand() {
        transform.parent = originalParent;
        havePlayer = false;
        supportNum = 1;
        //GetComponent<FixedJoint>().connectedBody = null;

    }


    public void positiontoOg() {

        transform.position = startPos;
            
    }


    public void removeCollider() {
        foreach (MeshCollider m in supportBarCollider) {
            m.enabled = false;
        }

        transform.position = startPos;
    }
    public void DisableCollider()
    {
        foreach (MeshCollider m in supportBarCollider)
        {
            m.enabled = false;
            m.convex = false;
        }
    }
    public void EnableCollider()
    {
        StartCoroutine(delay(0.2f, () => {
            supportBarCollider[supportNum - 1].enabled = true;
            //supportBarCollider[supportNum - 1].convex = true;
        }));
        
    }
    IEnumerator delay(float time,System.Action OnComplete)
    {
        yield return new WaitForSeconds(time);
        OnComplete?.Invoke();
    }
    public void addRigi() {
        Rigidbody r = gameObject.AddComponent<Rigidbody>();
        r.isKinematic = true;
    }



}
