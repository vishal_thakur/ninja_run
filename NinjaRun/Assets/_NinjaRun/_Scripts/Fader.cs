using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Fader : MonoBehaviour
{
    public float speed;
    public bool willDestroy = true;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FadeAway());   
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator FadeAway()
    {
        Color col = GetComponent<Image>().color;
        while (col.a > 0)
        {
            col.a -= speed * Time.deltaTime;
            GetComponent<Image>().color = col;
            Vector3 scalevec = transform.localScale;
            scalevec += Vector3.one * speed * Time.deltaTime;
            transform.localScale = scalevec;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        if(willDestroy)
        {
            Destroy(this.gameObject);
        }
        else
        {
            transform.localScale = Vector3.zero;
            GetComponent<Image>().color = Color.white;
            StartCoroutine(FadeAway());
        }
       
    }
}
