using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class LandingElement : MonoBehaviour
{
    [SerializeField]
    bool setDefaultCameraView = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            other.GetComponent<CharacterMovement>().ShowPerfectUI();
          //  Debug.LogError(other.name);


            if (setDefaultCameraView)
                GameObject.FindObjectOfType<CameraMovement>().ShowDefaultView();

        other.GetComponent<Rigidbody>().useGravity = true;
        other.GetComponent<Rigidbody>().isKinematic = false;
            other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.gameObject.transform.eulerAngles = Vector3.zero;
            //other.gameObject.GetComponent<Animator>().SetTrigger("groundLadder");
            //CameraMovement.instance.EnableCameraMotionForLadderland();
            CapsuleCollider capsuleCollider = other.gameObject.GetComponent<CapsuleCollider>();
            capsuleCollider.height = 1f;
            capsuleCollider.radius = 0.3f;
            capsuleCollider.center = new Vector3(0, 0.5f, 0);
            Timing.CallDelayed(1f, () =>
            {
                CameraMovement.instance.AllowFollow();
                other.gameObject.GetComponent<Animator>().ResetTrigger("groundLadder");
                other.gameObject.GetComponent<Animator>().SetBool("Run", true);
                GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterMovement>().canMove = true;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().isKinematic = false;

            });

           // GetComponent<BoxCollider>().enabled = false;
        }
    }
}
