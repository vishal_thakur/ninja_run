using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class GameAnalyticsController : MonoBehaviour
{
    #region SINGLETON_PATTERN
    //private static GameAnalyticsController _instance;
    public static GameAnalyticsController Instance;
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = FindObjectOfType<GameAnalyticsController>();
    //            if (_instance == null)
    //            {
    //                GameObject GameAnalyticsController = new GameObject("GameAnalyticsController");
    //                _instance = GameAnalyticsController.AddComponent<GameAnalyticsController>();
    //            }
    //            DontDestroyOnLoad(_instance.gameObject);
    //        }
    //        return _instance;
    //    }
    //}
    #endregion
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        GameAnalytics.Initialize();
    }

    public void OnLevelStart()
    {
        int level = Globals.LoadedLevel + 1;
    //    Debug.LogError("level start = " + level);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level" + level);
    }
    public void OnLevelComplete()
    {
        int level = Globals.LoadedLevel + 1;
  //      Debug.LogError("level Complete = " + level);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level1" + level);
    }
    public void OnLevelFail()
    {
        int level = Globals.LoadedLevel + 1;
//        Debug.LogError("level Fail = " + level);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level1" + level);
    }
}
