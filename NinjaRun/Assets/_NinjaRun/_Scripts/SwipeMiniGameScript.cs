using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeMiniGameScript : MonoBehaviour
{
    public Vector3 delta  = Vector3.zero;
    private Vector3 lastPos = Vector3.zero;
    public GameObject miniGameUI;
    public GameObject dragBarPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnDisable()
    {
        miniGameUI.SetActive(false);
        dragBarPanel.GetComponent<DragBar>().checkDrag = false;
        dragBarPanel.SetActive(false);
    }

    private void OnEnable()
    {
        dragBarPanel.GetComponent<DragBar>().checkDrag = true;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButton(0))
        {
            miniGameUI.SetActive(false);
            dragBarPanel.SetActive(true);
            float xAxis = Input.GetAxis("Mouse X");
            //if(Input.mousePosition.x > 600)
            //{
            //    Debug.Log("Drag Right");
            //    GetComponent<CharacterMovement>().SetShearValue(0.8f);
            //}
            //else
            //{
            //    Debug.Log("Drag Left");
            //    GetComponent<CharacterMovement>().SetShearValue(-0.8f);
            //}
            if (xAxis > 0.5)
            {
                GetComponent<CharacterMovement>().SetShearValue(0.8f);
                dragBarPanel.GetComponent<DragBar>().RightDrag();
            }
            else if (xAxis < -0.5)
            {
                GetComponent<CharacterMovement>().SetShearValue(-0.8f);
                dragBarPanel.GetComponent<DragBar>().leftDrag();
            }
        }
    }

    void MouseMove()
    {
        
    }
    void TouchMove()
    {
       //if (Input.touchCount == 1)
       //{
       //    Touch touch = Input.GetTouch(0);
       //    if (touch.phase == TouchPhase.Moved)
       //    {
       //        xAxis += touch.deltaPosition.x;
       //        desiredRotation = Quaternion.Euler(0, xAxis * rotationSpeedDevice, 0);
       //        currentRotation = transform.rotation;
       //        transform.rotation = Quaternion.Slerp(currentRotation, desiredRotation, smoothFactor);
       //    }
       //}
    }
}
