using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class FastTapMiniGame : MonoBehaviour
{
    public float decreaseDelta;
    public float increaseDelta;

    public Image progressBar;
    public float tapDelayEnd;
    public GameObject player;
    public GameObject invisbleWall;
    public GameObject touchFeedback;
    public Transform touchFeedbackParent;
    public GameObject InstructionsHolder;

    public UnityEvent MethodToInvokeOnTap;

    //[SerializeField]
    //bool isOldStyled = true;

   // bool hasDoneFirstTap;

    // Start is called before the first frame update
    void Start()
    {
        progressBar.fillAmount = 0;
    }

    private void OnEnable()
    {
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex < 2)
            player.GetComponent<CharacterMovement>().DecreaseSpeed();
        invisbleWall.SetActive(true);
        InstructionsHolder.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        float decreaseVal = (TapDelayEnded ? decreaseDelta * 3 : decreaseDelta);
        progressBar.fillAmount = Mathf.Clamp(progressBar.fillAmount - decreaseVal * Time.deltaTime, 0, 1);
    }

    public bool TapDelayEnded;

    public void OnTap()
    {
        //Only Applicabe In level 0, 1 not in 2 
        if (!player.GetComponent<CharacterMovement>().canTakeFastTap)
        {
            return;
        }


        //Invoke the following on tap
        //if (!hasDoneFirstTap && !isOldStyled)
        //{
        //    hasDoneFirstTap = true;
        //}

        StopAllCoroutines();
        TapDelayEnded = false;
        StartCoroutine(CheckforTapDelay());
        GameObject obj = (GameObject)Instantiate(touchFeedback, touchFeedbackParent);
        obj.transform.position = Input.mousePosition;
        progressBar.fillAmount += increaseDelta;
        InstructionsHolder.SetActive(false);

    }

    IEnumerator CheckforTapDelay()
    {
        yield return new WaitForSeconds(tapDelayEnd);
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex < 2)
            player.GetComponent<CharacterMovement>().SetFallAnim();
        TapDelayEnded = true;
    }

    //public void RemoveAllCallbacks()
    //{
    //    MethodToInvokeOnTap.RemoveAllListeners();
    //}
}
