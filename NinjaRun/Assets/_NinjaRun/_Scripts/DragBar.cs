using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DragBar : MonoBehaviour
{
    public bool checkDrag;
    public GameObject player;
    public Image leftDragBar, rightDragBar;
    public float barFillSpeed, barEmptySpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    bool isleft;
    private void OnEnable()
    {
        leftDragBar.fillAmount = 0.2f;
        rightDragBar.fillAmount = 0.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if(checkDrag)
        {
        if(isleft)
            {
                leftDragBar.fillAmount = Mathf.Clamp(leftDragBar.fillAmount + barFillSpeed * Time.deltaTime, 0.2f, 1f);
                rightDragBar.fillAmount = Mathf.Clamp(rightDragBar.fillAmount - barEmptySpeed * Time.deltaTime, 0.2f, 1f);
            }
        else
            {
                leftDragBar.fillAmount = Mathf.Clamp(leftDragBar.fillAmount - barEmptySpeed * Time.deltaTime, 0.2f, 1f);
                rightDragBar.fillAmount = Mathf.Clamp(rightDragBar.fillAmount + barFillSpeed * Time.deltaTime, 0.2f, 1f);
            }
        }
    }

    public void leftDrag()
    {
        //leftDragBar.fillAmount = Mathf.Clamp(leftDragBar.fillAmount + barFillSpeed * Time.deltaTime, 0.2f, 1f);
        //rightDragBar.fillAmount = Mathf.Clamp(rightDragBar.fillAmount - barEmptySpeed * Time.deltaTime, 0.2f, 1f);
        isleft = true;
    }

    public void RightDrag()
    {
        //leftDragBar.fillAmount = Mathf.Clamp(leftDragBar.fillAmount - barEmptySpeed * Time.deltaTime, 0.2f, 1f);
        //rightDragBar.fillAmount = Mathf.Clamp(rightDragBar.fillAmount + barFillSpeed * Time.deltaTime, 0.2f, 1f);
        isleft = false;
    }
}
