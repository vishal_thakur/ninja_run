using com.adjust.sdk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustController : MonoBehaviour
{
    #region SINGLETON_PATTERN
    //private static AdjustController _instance;
    public static AdjustController Instance;
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = FindObjectOfType<AdjustController>();
    //            if (_instance == null)
    //            {
    //                GameObject AdjustController = new GameObject("AdjustController");
    //                _instance = AdjustController.AddComponent<AdjustController>();
    //            }
    //            DontDestroyOnLoad(_instance.gameObject);
    //        }
    //        return _instance;
    //    }
    //}
    #endregion
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void OnLevelStart()
    {
        AdjustEvent adjustEvent = new AdjustEvent("LevelStart");
        Adjust.trackEvent(adjustEvent);
    }
    public void OnLevelComplete()
    {
        AdjustEvent adjustEvent = new AdjustEvent("LevelComplete");
        Adjust.trackEvent(adjustEvent);
    }
    public void OnLevelFail()
    {
        AdjustEvent adjustEvent = new AdjustEvent("LevelFail");
        Adjust.trackEvent(adjustEvent);
    }
}
