﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;


public class EnableMiniGameScript : MonoBehaviour
{

    public Globals.miniGame nameOftheGame;
    public bool shouldDeativate;
    public bool shouldStop;
    public GameObject miniGameUI;
    public bool slow;
    public float slowValue = 0.1f;
    public bool shouldDeactiveSelf = false;
    public bool changeToStartPos,lookatPlayer , cameraFollowsPlayer = false;

    //Represents a Camera View Tranform that the Camera will lerp to
    [SerializeField]
    Transform NewCameraViewTransform;

    [SerializeField]
    float lerpSpeed = 5;

    //Enabling this bool will Switch camera's view to side 
    //public bool CameraSideView = false, CameraZoomOutView = false;

    // Start is called before the first frame update
    void Start()
    {
        lookatPlayer = false;
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (lookatPlayer) {
    //        Camera.main.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
    //    }
    //}
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            if (changeToStartPos)
            {
                
                    Vector3.Lerp(other.transform.position, transform.position, 5);

            }
           
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            //IF New Camera View has been Assigned then Tell camera to Lerp to that View
            if (NewCameraViewTransform)
                GameObject.FindObjectOfType<CameraMovement>().ShowNewCameraView(NewCameraViewTransform , cameraFollowsPlayer , lerpSpeed);
            else
                GameObject.FindObjectOfType<CameraMovement>().followPlayer = cameraFollowsPlayer;


            //Save Current MiniGame
            Globals.CurrentMiniGame = nameOftheGame;

            //Debug.Log(transform.name+"trigger" + nameOftheGame);
            CharacterMovement characterMovement = other.gameObject.GetComponent<CharacterMovement>();
            if (nameOftheGame != Globals.miniGame.salmon )
            {
                if (!shouldDeativate)
                    CameraMovement.instance.EnableCameraMotion();
                else
                    CameraMovement.instance.DisableCameraMotion();

                if (shouldStop)
                {
                    characterMovement.StopMovement();
                }
            }
            else {
                //lookatPlayer = true;
                other.gameObject.GetComponent<Animator>().ResetTrigger("hung");
                characterMovement.EnableHandCollider();
                if (!shouldDeativate)
                {
                    CameraMovement.instance.EnableCameraMotionForSalmonLadder();
                }

                else {
                    CameraMovement.instance.DisableCameraMotionForSalmonLadder();
                    lookatPlayer = false;
                }
                   

                if (shouldStop)
                {
                    characterMovement.StopMovement();
                }
            }

            Timing.CallDelayed(0.5f, () =>
            {
                if (miniGameUI != null)
                {
                    if (miniGameUI.activeInHierarchy)
                    {
                        miniGameUI.SetActive(false);
                    }
                    else
                    {
                        miniGameUI.SetActive(true);
                    }
                    this.gameObject.SetActive(!shouldDeactiveSelf);
                }
            });


        if (slow)
            {
                characterMovement.moveSpeed = slowValue;
            }
        }
    }
}

//for salmonLadder
/*
   kill rb,
 */