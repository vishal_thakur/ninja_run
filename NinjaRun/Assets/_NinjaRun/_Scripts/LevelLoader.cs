using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{


    public void RestartCurrentLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(Globals.LoadedLevel);
    }

    public void LoadRandomLevel()
    {
        //Load Random Level Out of the available levels
        int randomLevel = Globals.GetRandomLevel();

        if (randomLevel == Globals.LoadedLevel)
            LoadRandomLevel();
        else
            UnityEngine.SceneManagement.SceneManager.LoadScene(randomLevel);
    }


    
    public void OnExit()
    {
        Application.Quit();
    }
}
