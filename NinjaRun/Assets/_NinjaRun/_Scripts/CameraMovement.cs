﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraMovement : MonoBehaviour
{
    public static CameraMovement instance;

    public Transform Player;
    public List<Transform> obstacle_Blendlists;
    public GameObject initialBlendList;
    public Vector3 diff;
    public bool followPlayer;
    public bool lookatPlayer;
    Camera _camera;
    Vector3 initialPos;
    Vector3 initialPlayerDiff;
    bool isLadder;


    //Represents the default/Orignal camera Transform
    [SerializeField]
    Transform OrignalCameraTransform;

    [SerializeField]
    Transform DeathCameraViewTransform;

    [SerializeField]
    Transform CameraRef;

    [SerializeField]
    float CameraViewSwitchSpeed = 2;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        _camera = transform.GetChild(0).GetComponent<Camera>();
        initialPos = transform.position;
        diff = Player.position - transform.position;
        initialPlayerDiff = diff;
    }

    public void changeDiff()
    {
        diff = Player.position - transform.position;
    }



    
    private void LateUpdate()
    {
        if(followPlayer)
            transform.position = Vector3.Lerp(transform.position , Player.position - new Vector3(diff.x - .2f , diff.y , 7.5f) , Time.deltaTime * 5.5f);

        //if(lookatPlayer)
        //{
        //    _camera.transform.LookAt(Player.transform);
        //}
    }
    public void StopFollow()
    {
        followPlayer = false;
    }

    public void StartFollowPlayer()
    {
        _camera.transform.localPosition = Vector3.zero;
        _camera.transform.localRotation = Quaternion.Euler(Vector3.zero);
        followPlayer = true;
    }
    public void AllowFollow(bool resetChildCam=false,bool ischangeDiff=true)
    {
        if (ischangeDiff)
            changeDiff();
        else
        {
            diff = initialPlayerDiff;
        }
        if(resetChildCam)
        {
            _camera.transform.localPosition = Vector3.zero;
            _camera.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
        followPlayer = true;
    }
    public void ResetCamParentPos()
    {
        if(isLadder)
        {
            Vector3 pos = transform.position;
            pos.y = initialPos.y;
            _camera.transform.SetParent(null);
            transform.position = pos;           
            _camera.transform.SetParent(this.transform);
            
            isLadder = false;
        }
        
    }
    public void EnableCameraPositionEnd()
    {
        obstacle_Blendlists[2].gameObject.SetActive(true);
    }

    public void EnableEndCameraMovement()
    {
        obstacle_Blendlists[2].gameObject.SetActive(false);
        obstacle_Blendlists[3].gameObject.SetActive(true);
    }

    public void EnableCameraMotion()
    {
        obstacle_Blendlists[0].gameObject.SetActive(true);
        obstacle_Blendlists[1].gameObject.SetActive(false);
        StartCoroutine(disableCurrentBlendList(0.3f, obstacle_Blendlists[0].gameObject));
    }

    public void EnableCameraMotionForSalmonLadder()
    {
        StopFollow();
        obstacle_Blendlists[4].gameObject.SetActive(true);
        obstacle_Blendlists[1].gameObject.SetActive(false);
        StartCoroutine(disableCurrentBlendList(0.3f, obstacle_Blendlists[4].gameObject,()=> {
            isLadder = true;
            Vector3 pos = transform.position;
            pos.y = _camera.transform.localPosition.y;
            _camera.transform.SetParent(null);
            transform.position = pos;
            _camera.transform.SetParent(this.transform);
        }));
    }
    public void EnableCameraMotionForLadderland()
    {
        HideAll();
        obstacle_Blendlists[5].gameObject.SetActive(true);
        StartCoroutine(disableCurrentBlendList(0.2f, obstacle_Blendlists[5].gameObject));
    }
    public void EnableCamMotionPipeDie()
    {
        HideAll();
        obstacle_Blendlists[7].gameObject.SetActive(true);
        StartCoroutine(disableCurrentBlendList(0.45f, obstacle_Blendlists[7].gameObject));
    }
    public void EnableCamMotionLadderDie()
    {
        HideAll();
        obstacle_Blendlists[6].gameObject.SetActive(true);
        StartCoroutine(disableCurrentBlendList(0.45f, obstacle_Blendlists[6].gameObject));
    }
    void HideAll()
    {
        for (int i = 0; i < obstacle_Blendlists.Count; i++)
        {
            obstacle_Blendlists[i].gameObject.SetActive(false);
        }
    }
    IEnumerator disableCurrentBlendList(float delay, GameObject obj,Action OnComplete=null)
    {
        yield return new WaitForSeconds(delay);
        obj.SetActive(false);
        OnComplete?.Invoke();
    }

    public void DisableCameraMotion()
    {
        obstacle_Blendlists[1].gameObject.SetActive(true);
        obstacle_Blendlists[0].gameObject.SetActive(false);
        StartCoroutine(disableCurrentBlendList(0.3f, obstacle_Blendlists[1].gameObject));

    }
    public void DisableCameraMotionForSalmonLadder()
    {
       
        obstacle_Blendlists[1].gameObject.SetActive(true);
        obstacle_Blendlists[4].gameObject.SetActive(false);
        StartCoroutine(disableCurrentBlendList(0.3f, obstacle_Blendlists[1].gameObject));

    }
    public void DisableCameraMotionFirst()
    {
        Invoke("DisableFirstCamera", 0.5f);
        Player.gameObject.GetComponent<CharacterMovement>().Run();
     //   GameAnalyticsController.Instance.OnLevelStart();
      //  AdjustController.Instance.OnLevelStart();
    }

    void DisableFirstCamera()
    {
        initialBlendList.SetActive(false);
        
    }

    //private void Update()
    //{
    //    if (Input.GetKeyUp(KeyCode.LeftArrow))
    //        ShowSideView();
    //    if (Input.GetKeyUp(KeyCode.RightArrow))
    //        ShowDefaultView();
    //}





    //Lerp to The Side View Camera Transfom
    //public void ShowSideView()
    //{
    //    followPlayer = false;
    //    StartCoroutine(LerpToTranform(CameraRef.transform, SideViewCameraTransform, false));
    //}



    ////Lerp to The Side View Camera Transfom
    //public void ShowZoomOutView()
    //{
    //    followPlayer = false;
    //    StartCoroutine(LerpToTranform(CameraRef.transform, ZoomOutViewCameraTransform, false));
    //}

    public void ShowDeathCameraView()
    {

        followPlayer = true;
        StopAllCoroutines();
        // Debug.LogError("Stop coroutines ShowDefaultView");
        // inLerpMode = false;
        //  this.StopAllCoroutines();
        // followPlayer = true;
        StartCoroutine(LerpToTranform(CameraRef.transform, DeathCameraViewTransform, true, CameraViewSwitchSpeed));
    }
    //bool inLerpMode = false;
    public void ShowNewCameraView(Transform newCameraView , bool CamerafollowsPlayer , float lerpSpeed)
    {
        followPlayer = CamerafollowsPlayer;
        //   Debug.LogError("Stop coroutines ShowNewCameraView");
        StopAllCoroutines();
    //    this.StopAllCoroutines();
       // inLerpMode = false;
        StartCoroutine(LerpToTranform(CameraRef.transform, newCameraView, CamerafollowsPlayer , lerpSpeed));
    }

    //Lerp to The Default Camera Transfom
    public void ShowDefaultView()
    {
        followPlayer = true;
        StopAllCoroutines();
        // Debug.LogError("Stop coroutines ShowDefaultView");
        // inLerpMode = false;
        //  this.StopAllCoroutines();
        // followPlayer = true;
        StartCoroutine(LerpToTranform(CameraRef.transform, OrignalCameraTransform, true , CameraViewSwitchSpeed));
    }



    IEnumerator LerpToTranform(Transform lerpObject, Transform target, bool followPlayerOnFinishFlag , float lerpSpeed)
    {
        // inLerpMode = true;
        //  Debug.LogError("Follow= " + followPlayerOnFinishFlag);
        float positionDifference = Vector3.SqrMagnitude(lerpObject.localPosition - target.localPosition);
        float rotationDifference = Vector3.SqrMagnitude(lerpObject.eulerAngles - target.eulerAngles);

        //Lerp Player Position To match the animation
        //Lerp to Target 
        while (positionDifference > 0.0001f & rotationDifference > 0.0001f)
        {
            //  Debug.LogError(distance.ToString());
            //Calculate distance between player and target
            positionDifference = Vector3.SqrMagnitude(lerpObject.localPosition - target.localPosition);
            rotationDifference = Vector3.SqrMagnitude(lerpObject.eulerAngles - target.eulerAngles);

            //Get player Position
            // playerPosition = PlayerRigidBodyRef.position;


            //Lerp To Target
            lerpObject.localPosition = Vector3.Lerp(lerpObject.localPosition, target.localPosition, Time.deltaTime * lerpSpeed);
            lerpObject.localRotation = Quaternion.Lerp(lerpObject.localRotation, target.localRotation, Time.deltaTime * lerpSpeed);
            yield return new WaitForEndOfFrame();
        }
        //StopCoroutine("LerpToTranform");
    }
}
