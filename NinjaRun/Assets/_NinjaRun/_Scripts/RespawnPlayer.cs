using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Events;

public class RespawnPlayer : MonoBehaviour
{
    bool hasRespawned = false;

    //If true will show Water Splash FX at the Collision point
    [SerializeField]
    bool showWaterSplashFX = false;

    [SerializeField]
    bool showDeathCam = false;

    //water Splash FX
    [SerializeField]
    GameObject WaterSplashFX;

    [SerializeField]
    float invokeDelay = 0;

    public UnityEvent MethodsToInvoke;


    public ToggleSwitch toggleSwitch;



    private void OnCollisionEnter(Collision collision)
    {
      //  Debug.LogError("OnCollisionEnter" + collision.collider.gameObject.name);
        if (collision.transform.root.tag.Equals("Player") && !hasRespawned)
        {
            if(showWaterSplashFX && WaterSplashFX)
            {
                WaterSplashFX.transform.position = collision.collider.transform.position;
                WaterSplashFX.gameObject.SetActive(true);
                showWaterSplashFX = false;
              //  Timing.CallDelayed(3 , ()=> WaterSplashFX.gameObject.SetActive(false));
            }

            if (invokeDelay > 0)
                Timing.CallDelayed(invokeDelay, () => MethodsToInvoke.Invoke());
            else
                MethodsToInvoke.Invoke();

            //Debug.LogError("Respawning , via = " + collision.collider.name);
            //if((int)Globals.CurrentMiniGame > 5)
            //{
            //    hasRespawned = true;
            //    Timing.CallDelayed( 2f , () =>GameObject.FindObjectOfType<GameManager>().RespawnPlayer());
            //}
            //else
            //{
            //if (Globals.LoadedLevel < 2)
            //    collision.gameObject.GetComponent<CharacterMovement>().SetDeath();
            //else if (Globals.LoadedLevel >= 2)
            //{
            //    hasRespawned = true;
            //    Timing.CallDelayed(2f, () => GameObject.FindObjectOfType<GameManager>().RespawnPlayer());
            //}

            toggleSwitch.VibrateForFail();
                GameAnalyticsController.Instance.OnLevelFail();
                AdjustController.Instance.OnLevelFail();
            if(showDeathCam)
                GameObject.FindObjectOfType<CameraMovement>().ShowDeathCameraView();
                //if (Globals.LoadedLevel == 1)
                //    GameObject.FindObjectOfType<SwingGameManager>().ToggleHingeJointsCollider(true);

            
            //}

        }
    }
}
