using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WinFX : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject winPanel;
    public GameObject gameplayPanel;
    [SerializeField] private List<GameObject> winConfettiList;

    [SerializeField]
    bool isOldGame = true;
    public UnityEvent MethodsToInvoke;

    void Start()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        MethodsToInvoke.Invoke();

        winPanel.SetActive(true);
        GetComponent<BoxCollider>().enabled = false;
        this.gameObject.SetActive(false);
        gameplayPanel.SetActive(false);


        for (int i = 0; i < winConfettiList.Count; i++)
        {
            winConfettiList[i].SetActive(true);
        }

        if (!isOldGame)
            return;




        CharacterMovement characterMovementRef = GameObject.FindObjectOfType<CharacterMovement>();


        if (characterMovementRef & !characterMovementRef.shear)
        {
            GameAnalyticsController.Instance.OnLevelComplete();
            AdjustController.Instance.OnLevelComplete();
            characterMovementRef.DoWin();
            CameraMovement.instance.EnableEndCameraMovement();
        }
    }
}
