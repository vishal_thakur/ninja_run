using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class launchUpwards : MonoBehaviour
{
    public float upwardForce;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(0,upwardForce,0);
        }
    }
}
