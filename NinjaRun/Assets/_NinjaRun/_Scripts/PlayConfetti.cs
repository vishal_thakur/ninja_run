using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayConfetti : MonoBehaviour
{
    public ParticleSystem confetti;
    // Start is called before the first frame update
    void Start()
    {
        //confetti = transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            confetti.Play();
        }
    }
}
