using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enableHanging : MonoBehaviour
{
    public bool entered;
    private void Start()
    {

        entered = false;
    }
    private void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        //First salmon Jump
        if (other.gameObject.tag.Equals("Player") && !entered)
        {
            other.gameObject.GetComponent<CharacterMovement>().EnableHanging();
            entered = true;
        }
    }
}
