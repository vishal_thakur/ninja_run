﻿
using System;
using UnityEngine;
using UnityEngine.UI;


    public class ToggleSwitch : MonoBehaviour
    {
        [SerializeField] private ToggleType type;
        [SerializeField] private ToggleHolder[] togglesHolder;
        [SerializeField] private ToggleStatus status;
        long[] longs;
    private void Awake()
    {
        Vibration.Init();
        string _text = "0, 30,25, 10,10";
        string[] patterns = _text.Replace(" ", "").Split(',');
         longs = Array.ConvertAll<string, long>(patterns, long.Parse);
        if (!PlayerPrefs.HasKey("vibration"))
        {
            PlayerPrefs.SetInt("vibration", 1);
            status = ToggleStatus.On;
        }else
        {
            status = PlayerPrefs.GetInt("vibration")==0?ToggleStatus.Off:ToggleStatus.On;
        }
        SetToggle(status == ToggleStatus.On);
    }
        public void Toggle()
        {
            ChangeToggleStatus();
        }

        public void SetOn(bool isNotNotification = false)
        {
            status = ToggleStatus.On;
            ToggleHolder toggleHolder = GetCorrectToggle(ToggleStatus.Off);

            for (int i = 0; i < toggleHolder.objects.Length; i++)
            {
                toggleHolder.objects[i].SetActive(false);
        }

            toggleHolder = GetCorrectToggle(ToggleStatus.On);

            for (int i = 0; i < toggleHolder.objects.Length; i++)
            {
                toggleHolder.objects[i].SetActive(true);
            }
            PlayerPrefs.SetInt("vibration", 1);
        //NotificationParam toggle = new NotificationParam(Mode.intData);
        //toggle.intData.Add((int)type);
        //if (!isNotNotification)
        //{
        //    App.Notify(Notification.ToggleOn, toggle);
        //}
    }

        public void SetOff(bool isNotNotification = false)
        {
            status = ToggleStatus.Off;
            ToggleHolder toggleHolder = GetCorrectToggle(ToggleStatus.Off);

            for (int i = 0; i < toggleHolder.objects.Length; i++)
            {
                toggleHolder.objects[i].SetActive(true);
            }

            toggleHolder = GetCorrectToggle(ToggleStatus.On);

            for (int i = 0; i < toggleHolder.objects.Length; i++)
            {
                toggleHolder.objects[i].SetActive(false);
            }
            PlayerPrefs.SetInt("vibration", 0);
        //NotificationParam toggle = new NotificationParam(Mode.intData);
        //toggle.intData.Add((int)type);
        //if (!isNotNotification)
        //{
        //    App.Notify(Notification.ToggleOff, toggle);
        //}
    }

        public ToggleType GetToggleType()
        {
            return type;
        }

        public void SetToggle(bool toggle)
        {
            if (toggle)
            {
                SetOn();
            }
            else
            {
                SetOff();
            }
        }

        private ToggleHolder GetCorrectToggle(ToggleStatus type)
        {
            ToggleHolder toggleHolder = null;

            for (int i = 0; i < togglesHolder.Length; i++)
            {
                if (togglesHolder[i].status == type)
                {
                    toggleHolder = togglesHolder[i];
                    break;
                }
            }

            return toggleHolder;
        }

        private void ChangeToggleStatus()
        {
            if (status == ToggleStatus.On)
            {
                SetOff();
            }
            else
            {
                SetOn();
            }
        }

    public void VibrateForFail()
    {
        if (status == ToggleStatus.On)
        {
           
            Debug.Log("vibrate");
            Vibration.Vibrate(longs,-1);
        }
    }
}


    public enum ToggleStatus
    {
        On,
        Off
    }

    [System.Serializable]
    public class ToggleHolder
    {
        public ToggleStatus status;
        public GameObject[] objects;
    }
    public enum ToggleType : int
    {
        Music = 0,
        SFX = 1,
        Vibration = 2,
    }

