﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CustomTween : MonoBehaviour
{
    public bool shouldDisable = true;
    public bool isMove = false;
    public float movePositionX;

    [SerializeField]
    float secondsToDisable = 1;

    [SerializeField]
    bool AnimateOnEnable = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        if(AnimateOnEnable)
            DoTween();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DoTween()
    {
        if(!isMove)
            transform.DOShakeScale(1f);
        else
            transform.DOLocalMoveX(movePositionX, 2f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);

        if(secondsToDisable > 0)
            Invoke("disableObject", secondsToDisable);
        
    }

    void disableObject()
    {
        if(!shouldDisable)
        {
            return;
        }
        transform.parent.gameObject.SetActive(false);
    }
}
