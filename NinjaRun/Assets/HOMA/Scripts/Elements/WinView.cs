﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using MEC;
//using UnityEngine.Animations.Rigging;
[System.Serializable]
public class SpeedRange
{
    public Vector3 startRot;
    public Vector3 endRot;
    public float angleDiff;
}
public class WinView : MonoBehaviour
{
    public float speedFactor;
    [SerializeField] private GameObject arrowContainer;

    public float[] speedRange;
    public SpeedRange[] angleRanges;
    Vector3 originalAngle;
    public Animator anim;
    public List<Animator> anim_handles;
    public GameObject MissClickPanel;
    public float barJumpForce;
    public float landJumpForce;
    public GameObject bar;
    public Transform landingPos;

    //public Rig Rig;

    public bool isTestModeOn;
    private void Start()
    {
        //Rig = anim.gameObject.transform.GetChild(2).gameObject.GetComponent<Rig>();
    }
    // Start is called before the first frame update
    private void OnEnable()
    {
        originalAngle = arrowContainer.transform.localEulerAngles;
        RotateArrow();
        //StartCoroutine(RotateArrowCo());
    }
    private void OnDisable()
    {
        arrowContainer.transform.DOKill();
        arrowContainer.transform.localEulerAngles = originalAngle;
    }
    void RotateArrow()
    {

        arrowContainer.transform.DOLocalRotate(new Vector3(0, 0, 44f), 2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);

    }
    IEnumerator RotateArrowCo()
    {
        bool isReverse = false;
        float t = 0f;
        Vector3 currentAngle = arrowContainer.transform.localEulerAngles;
        Vector3 targetAngle = angleRanges[0].startRot;
        while (t<2)
        {
            arrowContainer.transform.localEulerAngles = Vector3.Lerp(currentAngle,targetAngle,t);
            t += Time.deltaTime;
            yield return null;
        }
        t = 0f;
        currentAngle = arrowContainer.transform.localEulerAngles;
        targetAngle = angleRanges[angleRanges.Length-1].endRot;
        while (t < 2)
        {
            arrowContainer.transform.localEulerAngles = Vector3.Lerp(currentAngle, targetAngle, t);
            t += Time.deltaTime;
            yield return null;
        }
    }

    //fist salmon Jump slider calculator
    public void SetValue()
    {

        arrowContainer.transform.DOKill();
      
        float newAngle = angleRanges[0].startRot.z;
        for (int i = 0; i < angleRanges.Length; i++)
        {
            newAngle -= angleRanges[i].angleDiff;
            Debug.Log(newAngle + ">>>>>>>>>>>>>>>>" + arrowContainer.transform.rotation.eulerAngles);
            if(ChangeAngle(arrowContainer.transform.rotation.eulerAngles).z>=newAngle)
            {
                Debug.Log("index" + i);
                if (speedRange[i] == 2)
                {
                    if(Globals.CurrentMiniGame == Globals.miniGame.boxJump || Globals.CurrentMiniGame == Globals.miniGame.stairClimb 
                    || Globals.CurrentMiniGame == Globals.miniGame.spiderWall || Globals.CurrentMiniGame == Globals.miniGame.sideWall
                    || Globals.CurrentMiniGame == Globals.miniGame.broadway
                    || Globals.CurrentMiniGame == Globals.miniGame.swingChase)
                    {
                        GameObject.FindObjectOfType<GameManager>().LoadMiniGameBehaviour((int)Globals.CurrentMiniGame);
                    }
                    else {

                        anim.SetTrigger("jumpToHang");


                        ///normal jump
                        Timing.CallDelayed(0.5f, () => callJump());
                    }

                    MissClickPanel.SetActive(false);


                         //Invoke("callJump", .5f);
                    Invoke("RigRIG", .6f);
                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                }
                else
                {
                    if (Globals.CurrentMiniGame == Globals.miniGame.boxJump || Globals.CurrentMiniGame == Globals.miniGame.stairClimb 
                    || Globals.CurrentMiniGame == Globals.miniGame.spiderWall || Globals.CurrentMiniGame == Globals.miniGame.sideWall
                    || Globals.CurrentMiniGame == Globals.miniGame.swingChase)
                    {
                        GameObject.FindObjectOfType<GameManager>().LoadMiniGameBehaviour((int)Globals.CurrentMiniGame);
                        Timing.CallDelayed(1f , ()=> GameObject.FindObjectOfType<GameManager>().RagdollDeath());
                        //Globals.hasRespawned = true;
                        //Timing.CallDelayed(2, () => GameObject.FindObjectOfType<GameManager>().RespawnPlayer());
                    }
                    else if(Globals.CurrentMiniGame == Globals.miniGame.broadway)
                    {
                        GameObject.FindObjectOfType<GameManager>().DefeatedGameOver();
                    }
                    else
                    {
                        anim.SetTrigger("jumpToHang");
                        callJump(true);
                    }

                    MissClickPanel.SetActive(true);

                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                }
                break;
            }
        }
        return;
        
        for (int i = 0; i < angleRanges.Length; i++)
        {
            //Debug.Log((i > angleRanges.Length / 2) + "i"+ angleRanges.Length / 2);
            if (IsInRange(angleRanges[i], arrowContainer.transform.eulerAngles, i > angleRanges.Length / 2))
            {
                Debug.Log(speedRange[i]);
                if (speedRange[i] == 2)
                {

                    MissClickPanel.SetActive(false);


                    anim.SetTrigger("jumpToHang");
                    //for(int j = 0; j < anim_handles.Count; j++)
                    //{
                    //    anim_handles[j].enabled = true;
                    //}
                    Invoke("callJump",.5f);
                    Invoke("RigRIG", .6f);
                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                }
                else
                {
                    MissClickPanel.SetActive(true);
                }
                break;
            }
        }
    }
    void CallBarUp()
    {
        Bar.instance.supportNum += 1;
        MissClickPanel.SetActive(false);
        //Bar.instance.GoToOriginalParent();
        anim.SetTrigger("barUp");
        Timing.RunCoroutine(WaitAndCall("hungClimb", "BarUp", 0.7f,()=> {
            if (Bar.instance.connectedToSupport && Bar.instance.supportNum < 5)
            {
                anim.gameObject.GetComponent<CharacterMovement>().checkedTheBarSupportNumber();
            }
            DeactivateKinematic();
            bar.GetComponent<BoxCollider>().enabled = false;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, barJumpForce, 0, ForceMode.Impulse);
            Invoke("DeactivateKinematic", 1f);
            Invoke("EnableBoxColliderOfBar", .4f);
        }));
    }
    IEnumerator<float> WaitAndCall(string prevState,string curState,float delay,Action OnComplete=null)
    {
        var state = anim.GetCurrentAnimatorStateInfo(0);
        //Debug.Log("waitcall");
        yield return Timing.WaitForOneFrame;
        //Debug.Log(state.IsName(curState) + "state" + anim.GetCurrentAnimatorStateInfo(0).IsName(prevState));
        while (anim.GetCurrentAnimatorStateInfo(0).IsName(prevState))
        {
            //Debug.Log(state.IsName(curState) + "state" + state.IsName(prevState));
            yield return Timing.WaitForOneFrame;

        }
        //Debug.Log(state.IsName(curState) + "state"+ state.normalizedTime);
        while(anim.GetCurrentAnimatorStateInfo(0).IsName(curState) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime<delay)
        {
            yield return Timing.WaitForOneFrame;
        }
        OnComplete?.Invoke();
    }
    //first support Jump
    public void SetValue1()
    {
        arrowContainer.transform.DOKill();
        float newAngle = angleRanges[0].startRot.z;
        for (int i = 0; i < angleRanges.Length; i++)
        {
            newAngle -= angleRanges[i].angleDiff;
            Debug.Log(newAngle + ">>>>"+ angleRanges[i].angleDiff + ">>>>"+transform.name+">>>>>>>>" + ChangeAngle(arrowContainer.transform.rotation.eulerAngles));
            if (ChangeAngle(arrowContainer.transform.rotation.eulerAngles).z >= newAngle)
            {
                Debug.Log("index" + i);
                if (speedRange[i] == 2 || isTestModeOn)
                {
                    CallBarUp();                  

                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    this.gameObject.SetActive(false);
                   
                }
                else
                {
                    DeactivateKinematic();
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, 2, 0, ForceMode.Impulse);
                    Invoke("DeactivateKinematic", 1f);
                    Invoke("EnableBoxColliderOfBar", 1.5f);

                    anim.SetTrigger("fallFromSalmonLadder");


                    //Rig.weight = 0;


                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    MissClickPanel.SetActive(true);
                    Bar.instance.removeFromHand();
                    Bar.instance.playerDead = true;
                    //FallFromLadder();
                }
                break;
            }
        }
        //if (isTestModeOn)
            return;
        
        for (int i = 0; i < angleRanges.Length; i++)
        {
            Debug.Log(angleRanges[i].startRot + "i" + angleRanges[i].endRot + "angle" + ChangeAngle(arrowContainer.transform.localEulerAngles));
            if (IsInRange(angleRanges[i], arrowContainer.transform.localEulerAngles, i > angleRanges.Length / 2))
            {
                Debug.Log(speedRange[i]);
                if (speedRange[i] == 2  || isTestModeOn)
                {
                    CallBarUp();
                    
            
                    if (Bar.instance.connectedToSupport && Bar.instance.supportNum < 5)
                    {
                        anim.gameObject.GetComponent<CharacterMovement>().checkedTheBarSupportNumber();
                    }
                
                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                }
                else
                {
                    DeactivateKinematic();
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, 2, 0, ForceMode.Impulse);
                    Invoke("DeactivateKinematic", 1f);
                    Invoke("EnableBoxColliderOfBar", 1.5f);

                    anim.SetTrigger("fallFromSalmonLadder");

                   

                    //Rig.weight = 0;
                    

                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    MissClickPanel.SetActive(true);
                    Bar.instance.removeFromHand();

                    //FallFromLadder();
                }
                break;
            }
        }
    }

    public void SetValue2()
    {
        arrowContainer.transform.DOKill();
        float newAngle = angleRanges[0].startRot.z;
        for (int i = 0; i < angleRanges.Length; i++)
        {
            newAngle -= angleRanges[i].angleDiff;
            //Debug.Log(newAngle + ">>>>>>>>>>>>>>>>" + arrowContainer.transform.rotation.eulerAngles);
            if (ChangeAngle(arrowContainer.transform.rotation.eulerAngles).z >= newAngle)
            {
                Debug.Log("index" + i);
                if (speedRange[i] == 2 || isTestModeOn)
                {
                    CapsuleCollider capsuleCollider = anim.gameObject.GetComponent<CapsuleCollider>();
                    capsuleCollider.height = 0.3f;
                    capsuleCollider.radius = 0.15f;
                    capsuleCollider.center = new Vector3(0, 0.6f, 0);
                    anim.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    anim.gameObject.GetComponent<CharacterMovement>().DisableHandCollider();
                    Bar.instance.GoToOriginalParent();
                    MissClickPanel.SetActive(false);
                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    //play Animation
                    anim.SetTrigger("barOff");


                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    //Invoke("goToLandingPos", 1f);

                    //Rig.weight = 0;


                    Timing.RunCoroutine(WaitAndCall("hungClimb", "barOff", 0.45f, () =>
                    {
                        anim.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                        anim.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, 0, landJumpForce, ForceMode.Impulse);
                    }));
                }else
                {
                    DeactivateKinematic();
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, 2, 0, ForceMode.Impulse);
                    Invoke("DeactivateKinematic", 1f);
                    Invoke("EnableBoxColliderOfBar", 1f);
                    anim.SetTrigger("fallFromSalmonLadder");

                    //Rig.weight = 0;

                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    MissClickPanel.SetActive(true);
                    Bar.instance.removeFromHand();
                    Bar.instance.playerDead = true;

                    //FallFromLadder();
                }
                break;
            }
        }

        //if (isTestModeOn)
            return;
        
        for (int i = 0; i < angleRanges.Length; i++)
        {
            //Debug.Log((i > angleRanges.Length / 2) + "i"+ angleRanges.Length / 2);
            if (IsInRange(angleRanges[i], arrowContainer.transform.eulerAngles, i > angleRanges.Length / 2))
            {
                Debug.Log(speedRange[i]);
                if (speedRange[i] == 2 || isTestModeOn)
                {
                    // Invoke("offRigRIG", .5f);
                    CapsuleCollider capsuleCollider = anim.gameObject.GetComponent<CapsuleCollider>();
                    capsuleCollider.height = 0.3f;
                    capsuleCollider.radius = 0.15f;
                    capsuleCollider.center = new Vector3(0, 0.6f, 0);
                    anim.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    anim.gameObject.GetComponent<CharacterMovement>().DisableHandCollider();
                    Bar.instance.GoToOriginalParent();
                    MissClickPanel.SetActive(false);
                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    //play Animation
                    anim.SetTrigger("barOff");
                    

                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    //Invoke("goToLandingPos", 1f);

                    //Rig.weight = 0;


                    Timing.RunCoroutine(WaitAndCall("hungClimb", "barOff", 0.45f, () =>
                    {
                        anim.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                        anim.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, 0, landJumpForce, ForceMode.Impulse);
                    }));




                }
                else
                {

                    DeactivateKinematic();
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().AddForce(0, 2, 0, ForceMode.Impulse);
                    Invoke("DeactivateKinematic", 1f);
                    Invoke("EnableBoxColliderOfBar", 1f);
                    anim.SetTrigger("fallFromSalmonLadder");

                

                    //Rig.weight = 0;


                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    MissClickPanel.SetActive(true);
                    Bar.instance.removeFromHand();
                    Bar.instance.playerDead = true;

                    //FallFromLadder();
                }
                break;
            }
        }
    }

    Vector3 ChangeAngle(Vector3 _angle)
    {
        if (_angle.z > 180)
        {
            _angle.z -= 360;
        }
        return _angle;
    }
    bool IsInRange(SpeedRange _angleRange, Vector3 _angle, bool reverse = false)
    {
        if (reverse)
        {
            Debug.Log(_angle.z);
            if (_angle.z > 180)
            {
                _angle.z -= 360;
            }
            Debug.Log("after" + _angle.z);

        }
        if ((_angleRange.startRot.z >= _angle.z || IsApproximately(_angleRange.startRot.z,_angle.z,0.1f)) && 
            (_angleRange.endRot.z <= _angle.z || IsApproximately(_angleRange.endRot.z, _angle.z, 0.1f)))
        {
            return true;
        }
        //if(reverse && _angleRange.startRot.z < _angle.z && _angleRange.endRot.z > _angle.z)
        //{
        //    return true;
        //}
        return false;
    }

    public bool IsApproximately(float a, float b, float tolerance)
    {
        //Debug.Log(Mathf.Abs(a - b));
        return Mathf.Abs(a - b) < tolerance;
    }
    void callJump(bool isFail=false)
    {
        anim.gameObject.GetComponent<CharacterMovement>().Jump(isFail);
    }

    void DeactivateKinematic() {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().isKinematic = false;

    }
    void ActivateKinematic()
    {

    }
    void EnableBoxColliderOfBar() {
        bar.GetComponent<BoxCollider>().enabled = true;
        anim.SetTrigger("hung");
        Debug.Log("hung anim");
        //Bar.instance.EnableCollider();
    }

    void goToLandingPos()
    {

        if(landingPos != null)
        {
            GameObject.FindGameObjectWithTag("Player").transform.position = landingPos.position;
            DeactivateKinematic();
            anim.SetBool("Run",true);
            GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterMovement>().canMove = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    void RigRIG() {
        //Rig.weight = 1;

    }
    void offRigRIG() {
        //Rig.weight = 0;

    }


    void FallFromLadder() {
        
        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        //remove collider from support bar
        


    }



    public void OnRockClimbGameStarted()
    {
        arrowContainer.transform.DOKill();

        float newAngle = angleRanges[0].startRot.z;
        for (int i = 0; i < angleRanges.Length; i++)
        {
            newAngle -= angleRanges[i].angleDiff;
            Debug.Log(newAngle + ">>>>>>>>>>>>>>>>" + arrowContainer.transform.rotation.eulerAngles);
            if (ChangeAngle(arrowContainer.transform.rotation.eulerAngles).z >= newAngle)
            {
                Debug.Log("index" + i);
                //Correct Click
                if (speedRange[i] == 2)
                {
                    //Signal RockClimbGame Manager to make  a jump to next Target
                    //Timing.CallDelayed(0.5f, () => GameObject.FindObjectOfType<RockClimbGameManager>().JumpToClimbHandle(null, false));

                    MissClickPanel.SetActive(false);
                    Invoke("RigRIG", .6f);
                    anim.gameObject.GetComponent<CharacterMovement>().ShowPerfectUI();
                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                    GameObject.FindObjectOfType<RockClimbGameManager>().JumpToClimbHandle(null);
                }
                //Wrong Click
                else
                {
                    // anim.SetTrigger("jumpToHang");
                    //Timing.CallDelayed(0.5f, () => GameObject.FindObjectOfType<RockClimbGameManager>().FailedJump());
                    GameObject.FindObjectOfType<RockClimbGameManager>().OnJumpFailed();
                    MissClickPanel.SetActive(true);
                    callJump(true);
                    this.gameObject.SetActive(false);
                    arrowContainer.transform.DOKill();
                }
                break;
            }
        }
    }


}
