using UnityEngine;
using UnityEngine.UI;

public class MiniGameBase : MonoBehaviour
{
    //Represents the Animator Trigger Name
    public string AnimatorTriggerName;
    //Reprsents this Mingame
    public Globals.miniGame miniGame;

    [SerializeField]
    protected GameObject ControlsUI;

    //If player Dies then he will respawn at this point
    public Transform RespawnPoint;

    //Player Needs to be at these positions during start and end to be able to play root motion animations in perfect sync with respective minigame's environment
    public Transform StartPoint, EndPoint;

    [SerializeField]
    protected GameObject RightMoveGraphic , WrongMoveGraphic;


    //  public int miniGameIndex = 0;

    public virtual void Initialize(bool showUI)
    {
        //Enable this MiniGameManager
        this.gameObject.SetActive(true);

        //Show Controls UI as per flag
        ControlsUI.SetActive(showUI);
    }

    public virtual void HideUI()
    {
        //Hide Controls UI
        ControlsUI.SetActive(false);
    }

    public virtual void Finish()
    {
        StopAllCoroutines();
        HideUI();
        this.gameObject.SetActive(false);
    }

    public virtual void OnDrawGizmos()
    {
        if (StartPoint)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawCube(StartPoint.position, .5f * Vector3.one);
        }
        if (EndPoint)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(EndPoint.position, .5f * Vector3.one);
        }
        if (RespawnPoint)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawCube(RespawnPoint.position, .5f * Vector3.one);
        }
    }
}
