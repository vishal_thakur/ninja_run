using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using UnityEngine.Events;

public class TriggerAction : MonoBehaviour
{
    [SerializeField]
    float MethodInvokeDelay = 0;

    //[SerializeField]
    //bool disableCollider = true;

    [SerializeField]
    public UnityEvent methodToExecute;

    //Represents a Camera View Tranform that the Camera will lerp to
    [SerializeField]
    Transform NewCameraViewTransform;
    
    [SerializeField]
    bool cameraFollowsPlayer = false;

    [SerializeField]
    float CameraLerpSpeed = 5;

    private void OnTriggerEnter(Collider other)
    {
        //Only execute If triggered by player
        if (other.gameObject.tag == "Player")
        {
            if (MethodInvokeDelay > 0)
                Timing.CallDelayed(MethodInvokeDelay, () => behaviour());
            else
                behaviour();
        }
    }

    void behaviour()
    {
        //GetComponent<BoxCollider>().enabled = false;
       // gameObject.SetActive(false);
        //Debug.LogError("moving camera to  " + NewCameraViewTransform.name);
        //IF New Camera View has been Assigned then Tell camera to Lerp to that View
        if (NewCameraViewTransform)
        {
            //GameObject.FindObjectOfType<CameraMovement>().StopAllCoroutines();
            GameObject.FindObjectOfType<CameraMovement>().ShowNewCameraView(NewCameraViewTransform, cameraFollowsPlayer, CameraLerpSpeed);
        }
        //else
        //    GameObject.FindObjectOfType<CameraMovement>().followPlayer = cameraFollowsPlayer;

        if(methodToExecute != null)
            methodToExecute.Invoke();

        //Timing.CallDelayed(4 , () => GetComponent<BoxCollider>().enabled = true);
        
    }
}
