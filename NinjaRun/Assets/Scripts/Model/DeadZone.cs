using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            //Gameover
            GameObject.FindObjectOfType<CharacterMovement>().GetComponent<Animator>().SetTrigger("death");
            GameObject.FindObjectOfType<CharacterMovement>().ShowMissPanel();
        }
    }
}
