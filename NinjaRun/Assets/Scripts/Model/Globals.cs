using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Globals
{
    #region Enumerations

    //Unique names of each level
    public enum LevelUniqueName
    {
        //Level 1
        Salmon = 0,

        //Level 2
        RockSwinger = 1,

        //Level 3
        BoxJumper = 2,

        //Level 4
        SpiderWall = 3,

        //Level 5
        SwingChaser = 4
    }


    //Enum of all possible Minigames in NinjaRun
    public enum miniGame
    {
        none = 0,
        //Level 1
        pipe = 1,
        salmon = 2,
        ramp = 3,

        //Level 2
        swingJump = 4,
        rockClimb = 5,

        //Level 3
        boxJump = 6,
        stairClimb = 7,

        //Level 4
        spiderWall = 8,
        sideWall = 9,

        //Level 5
        broadway = 10,
        swingChase = 11
    }
    #endregion



    #region Parameters

    //Represents the Currently active minigame
    public static miniGame CurrentMiniGame;

    public static int ApplicationTargetFrameRate = 60;
    //if True, then when Level Restarts "Start" button wont show up
    //If false normal Restart from
    public static bool hasRespawned = false;

    public static bool StartLoadingRandomLevels = false;


    //Represents the Max Swing AV of the hinge joint's rigidbody
    public static float SwingRingMaxAngularVelocity = 3f;


    //Represents the Total Levels in this Game
    public static int TotalLevels = 9;//UnityEngine.SceneManagement.SceneManager.sceneCount;

    //Represents the Currently loaded level buildIndexs
    public static int LoadedLevel = 0;

    public static int LevelToLoad = 0;

    //When A Time based button Comes up, the current minigame animation is reduced to this speed, if player manages to press the button at the right time. then it is reset to 1
    public static float MiniGameAnimSpeedReductionMultiplier = 1f;

    //
    public static float TimeToShowRightMoveVisual = .3f;

    public static bool hasShownSplash = false;
    public static bool hasCheckedSavedLevelLogic = false;

    #endregion


    #region Utility
    public static int GetRandomLevel() {



        if(StartLoadingRandomLevels)
        {
            int randomLevel = UnityEngine.Random.Range(1, TotalLevels);

            //Save Last level
            PlayerPrefs.SetInt("lastLevel" , randomLevel);
            return randomLevel;
        }
        else
        {
            LevelToLoad++;

            //keep loading levels in a sequnce
            if (LevelToLoad < TotalLevels)
            {
                //Save Last level
                PlayerPrefs.SetInt("lastLevel", LevelToLoad);
                return LevelToLoad;
            }
            //If Final level then Next time start loading random levels
            else if (LevelToLoad == TotalLevels)
            {
                StartLoadingRandomLevels = true;
                //Save Last level
                PlayerPrefs.SetInt("lastLevel", LevelToLoad);
                return LevelToLoad;
            }
        }

        Debug.LogError("Somethig wrong with level loading");
        //Save Last level
        PlayerPrefs.SetInt("lastLevel", 0);
        return 0;

    }

    public static void Vibrate()
    {
        Vibration.Init();
        string _text = "0, 30,25, 10,10";
        string[] patterns = _text.Replace(" ", "").Split(',');
        Vibration.Vibrate(System.Array.ConvertAll<string, long>(patterns, long.Parse), -1);
    }
    #endregion
}
