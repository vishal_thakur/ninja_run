using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndTemp : MonoBehaviour
{
    //// Start is called before the first frame update
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}

    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<Rigidbody>().useGravity = false;
        other.GetComponent<Rigidbody>().isKinematic = true;
        other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x, -0.209f, other.gameObject.transform.position.z);
        other.gameObject.transform.eulerAngles = Vector3.zero;
        other.transform.root.GetComponent<Animator>().SetTrigger("Run");

    }
}
