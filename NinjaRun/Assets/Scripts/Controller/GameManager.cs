using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    #region Parameters
    
    //Array of All minigames in current Level
    [SerializeField]
    List<MiniGameBase> MiniGamesInThisLevel = new List<MiniGameBase>();

    [Space(5)]
    //Represents the Currently active Minigame
    [SerializeField]
    MiniGameBase currentMiniGameInfo;

    //Ref to Player
    [SerializeField]
    GameObject PlayerRef;


    //UI Ref
    [SerializeField]
    GameObject beginPanelUI , gameplayPanelUI;

    //Perfect and Missed UI
    [SerializeField]
    GameObject perfectPanelRef, tryAgainPanelRef;

    //Player Animator Component Ref
    //[SerializeField]
    public Animator playerAnimator;

    //Represents the Text co
    [SerializeField]
    UnityEngine.UI.Text CurrentLevelUniqueNameText;



    [Space(10)]
    //List of all Rigidbodys that make up for the Player's Ragdoll
    [SerializeField]
    List<Rigidbody> PlayerRagdollRigidbodies = new List<Rigidbody>();


    #endregion

    private void Awake()
    {
        Application.targetFrameRate = Globals.ApplicationTargetFrameRate;

        //Load Last played Level
        //Save the Currently loaded level

        // int levelToLoad = 0;//= UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        if (!Globals.hasCheckedSavedLevelLogic)
        {
            //Check if there is saved last level
            if (PlayerPrefs.HasKey("lastLevel"))
            {
                //Get the saved last level
                Globals.LoadedLevel = PlayerPrefs.GetInt("lastLevel");

                //Load the saved Last Level
                UnityEngine.SceneManagement.SceneManager.LoadScene(Globals.LoadedLevel);
            }//if not then Do nothing
            else
            {
                Globals.LoadedLevel = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
            }

            Globals.hasCheckedSavedLevelLogic = true;
        }

    }

    #region Core
    void Start()
    {

        //SDK Callbacks
        GameAnalyticsController.Instance.OnLevelStart();
        AdjustController.Instance.OnLevelStart();


        //Show level Unique name
        CurrentLevelUniqueNameText.text = "" + (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex  + 1);//GetCurrentLevelUniqueName();


        //We have not implement this Scipt according to Level 1
        if (Globals.LoadedLevel == 0)
            return;


        //Check if is a Respawn
        if (Globals.hasRespawned)
        {
            LoadCurrentMiniGameInfo();
            if (currentMiniGameInfo)
            {
//                Debug.LogError("Current game = " + currentMiniGameInfo.gameObject.name);
                //Repositon Player at the current minigame's Respawn point
                PlayerRef.transform.position = currentMiniGameInfo.RespawnPoint.position;
            }

            //Start Game
            StartGame();
        }
    }

  

    public void StartGame()
    {
        //Reset has Respawned Param
        Globals.hasRespawned = false;

        //Reset Current mini game as none
        Globals.CurrentMiniGame = Globals.miniGame.none;
        currentMiniGameInfo = null;

        //Hide Begin panel UI
        beginPanelUI.SetActive(false);

        //Show Gameplay panel
        gameplayPanelUI.SetActive(true);

        //Reset Player Rotation to zero
        PlayerRef.transform.eulerAngles = Vector3.zero;

        //Signal Player Animator to Start Running
        StartCoroutine(SetAnimatorTrigger("run"));


    }


    public void RagdollDeath()
    {
        //SDK Callbacks
        GameAnalyticsController.Instance.OnLevelFail();
        AdjustController.Instance.OnLevelFail();

        PlayerRef.GetComponent<Rigidbody>().isKinematic = false;

        //Disable Player Collider
        PlayerRef.GetComponent<BoxCollider>().enabled = false;

        //Disable Camera Follow
        GameObject.FindObjectOfType<CameraMovement>().followPlayer = false;

        //Show Ragdoll 
        Timing.CallDelayed(.25f, () => StartCoroutine(SwitchFromAnimatorToRagdoll()));

        ShowTryAgainPanel();
    }

    public void DefeatedGameOver()
    {
        Vector3 targetPosition = new Vector3(PlayerRef.transform.position.x, PlayerRef.transform.position.y, PlayerRef.transform.position.z - .28f);
        //lerp Player a bit back in Z axis, prevents overlapping in Broadway Creator Game
        Timing.CallContinuously(2 , ()=> PlayerRef.transform.position = Vector3.Lerp(PlayerRef.transform.position , targetPosition , Timing.DeltaTime * 8));

        //Signal Player Animator to Start Running
        StartCoroutine(SetAnimatorTrigger("defeat"));

        //Disable Camera Follow
        GameObject.FindObjectOfType<CameraMovement>().followPlayer = false;

        ShowTryAgainPanel();

        Globals.hasRespawned = false;

        Timing.CallDelayed(2, () => RespawnPlayer());
    }


    public void RespawnPlayer()
    {
        //Dont Respawn if already Respawned
        if (Globals.hasRespawned)
            return;


        Globals.hasRespawned = true;

        //SDK Callbacks
        GameAnalyticsController.Instance.OnLevelFail();
        AdjustController.Instance.OnLevelFail();

        //hide Gameplay UI
        gameplayPanelUI.SetActive(false);


        //Restart Level at the Specified Location
        GameObject.FindObjectOfType<LevelLoader>().RestartCurrentLevel();


    }



    public void ShowPerfectPanel()
    {
        perfectPanelRef.SetActive(false);
        perfectPanelRef.SetActive(true);
    }

    public void ShowTryAgainPanel()
    {
        tryAgainPanelRef.SetActive(false);
        tryAgainPanelRef.SetActive(true);
    }

    #endregion




    #region Callbacks
    //On a specified Mini Game Started
    public void OnMiniGameStarted(int miniGameIndex)
    {
        //If Same MiniGame Started then donot do anything
        if (Globals.CurrentMiniGame == (Globals.miniGame)miniGameIndex)
            return;

        //Save the newly Started miniGame
        Globals.CurrentMiniGame = (Globals.miniGame)miniGameIndex;

        //Show level Unique name
        //CurrentLevelUniqueNameText.text = Globals.CurrentMiniGame.ToString();

        //Load CurrentMiniGame info
        LoadCurrentMiniGameInfo();


        //Reposition player to the currentMiniGame's Start point, this avoids rootMotion/Environment Sync errors
        Timing.CallContinuously( 1 , ()=> PlayerRef.transform.position = Vector3.Lerp(PlayerRef.transform.position  , currentMiniGameInfo.StartPoint.position , Timing.DeltaTime * 15));


        ////Do something Specific before Loading miniGame Behaviour
        //switch ((Globals.miniGame)miniGameIndex)
        //{

        //    case Globals.miniGame.ramp:
        //        //Initialize Final Minigame Controls Directly
        //        LoadMiniGameBehaviour((int)Globals.miniGame.ramp);
        //        //currentMiniGameInfo.Initialize(true);
        //        break;
        //}

        //Play Animation
        StartCoroutine(SetAnimatorTrigger("idle"));
    }


    public void OnLevelComplete()
    {
        //SDK Callbacks
        GameAnalyticsController.Instance.OnLevelComplete();
        AdjustController.Instance.OnLevelComplete();

        //Rotate Player Towards Camera
        PlayerRef.transform.eulerAngles = new Vector3(0, 180, 0);

        //Do Victory Dance
        StartCoroutine(SetAnimatorTrigger("dance"));
    }

    //Load the specified MiniGame's Behaviour
    public void LoadMiniGameBehaviour(int miniGameToStart)
    {

        switch ((Globals.miniGame)miniGameToStart)
        {
            case Globals.miniGame.boxJump:
                //Signal Player Animator
                StartCoroutine(SetAnimatorTrigger(currentMiniGameInfo.AnimatorTriggerName));

              //  currentMiniGameInfo = MiniGamesInThisLevel[0];
                break;

            case Globals.miniGame.stairClimb:

              //  currentMiniGameInfo = MiniGamesInThisLevel[1];

                //Signal Player Animator
                StartCoroutine(SetAnimatorTrigger(currentMiniGameInfo.AnimatorTriggerName));
                break;

            case Globals.miniGame.ramp:
                PlayerRef.GetComponent<Rigidbody>().isKinematic = false;
                //  currentMiniGameInfo = MiniGamesInThisLevel[2];
                //Signal Player Animator
                StartCoroutine(SetAnimatorTrigger(currentMiniGameInfo.AnimatorTriggerName));
                break;

            case Globals.miniGame.spiderWall:
                //   currentMiniGameInfo = MiniGamesInThisLevel[0];
                //Signal Player Animator
                StartCoroutine(SetAnimatorTrigger(currentMiniGameInfo.AnimatorTriggerName));
                break;

            case Globals.miniGame.sideWall:
                //   currentMiniGameInfo = MiniGamesInThisLevel[1];

                //Signal Player Animator
                StartCoroutine(SetAnimatorTrigger(currentMiniGameInfo.AnimatorTriggerName));
                break;

            case Globals.miniGame.broadway:
                // currentMiniGameInfo = MiniGamesInThisLevel[0];
                //Initialize Current Minigame
                //currentMiniGameInfo.Initialize(true);
                break;

            case Globals.miniGame.swingChase:
               // currentMiniGameInfo = MiniGamesInThisLevel[1];
                //Initialize Current Minigame
                //currentMiniGameInfo.Initialize(true);
                StartCoroutine(SetAnimatorTrigger(currentMiniGameInfo.AnimatorTriggerName));
                //playerAnimator.SetTrigger(Animator.StringToHash("miniGame_2"));
                break;
        }

        //Initialize Current Minigame
        currentMiniGameInfo.Initialize(true);
    }

    //On a specified Mini Game Started
    public void OnMiniGameEnded()
    {

        switch (Globals.CurrentMiniGame)
        {
            case Globals.miniGame.ramp:
                perfectPanelRef.gameObject.SetActive(true);
                currentMiniGameInfo.Finish();
                Timing.CallDelayed(3, () => RampGameEndBehaviour());
                return;

            case Globals.miniGame.swingChase:
                SwingChaseGameEndBehaviour();
                return;

        }

        //Reposition player to the currentMiniGame's Start point, this avoids rootMotion/Environment Sync errors
        Timing.CallContinuously(.5f, () => PlayerRef.transform.position = Vector3.Lerp(PlayerRef.transform.position, currentMiniGameInfo.EndPoint.position, Timing.DeltaTime * 10));

        //Signal Player Animator
        StartCoroutine(SetAnimatorTrigger("idle"));

        //Signal Player Animator
        Timing.CallDelayed(.5f, () => StartCoroutine(SetAnimatorTrigger("run")));

        ShowPerfectPanel();

        if (currentMiniGameInfo)
            currentMiniGameInfo.HideUI();

        //Reset Current mini game as none
        Globals.CurrentMiniGame = Globals.miniGame.none;

    }

    void RampGameEndBehaviour()
    {
        //Signal Player Animator
        StartCoroutine(SetAnimatorTrigger("run"));

        //Reset Current mini game as none
        Globals.CurrentMiniGame = Globals.miniGame.none;
    }

    void SwingChaseGameEndBehaviour()
    {
        //Disable Gravity and enable Kinematics
        PlayerRef.GetComponent<Rigidbody>().useGravity = false;
        PlayerRef.GetComponent<Rigidbody>().isKinematic = true;

        //Show perfectpanel
        ShowPerfectPanel();

        //Finish minigame
        currentMiniGameInfo.Finish();

        //Reset position and rotation to a valid one
        PlayerRef.transform.position = new Vector3(PlayerRef.transform.position.x,0 , PlayerRef.transform.position.z);
        PlayerRef.transform.eulerAngles = Vector3.zero;

        //Re-Enable Root motion
        playerAnimator.applyRootMotion = true;

        //Signal Player Animator
        StartCoroutine(SetAnimatorTrigger("idle"));

        //Signal Player Animator
        Timing.CallDelayed(.5f, () => StartCoroutine(SetAnimatorTrigger("run")));

        //Reset Current mini game as none
        Globals.CurrentMiniGame = Globals.miniGame.none;

    }
    #endregion







    #region Coroutines
    IEnumerator SwitchFromAnimatorToRagdoll()
    {
        yield return new WaitForEndOfFrame();

        //Make all Player's Ragdoll units as Non Kinematic
        foreach (Rigidbody rigidbody in PlayerRagdollRigidbodies)
        {
            rigidbody.isKinematic = false;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForEndOfFrame();

        //Disable Animator so that Ragdoll Physics can take over and do it's thing
        playerAnimator.enabled = false;

        StopCoroutine(SwitchFromAnimatorToRagdoll());
    }

    IEnumerator SetAnimatorTrigger(string name)
    {
        yield return null;
        //Signal Player Animator to Stop and Stay at Idle 
        playerAnimator.SetTrigger(Animator.StringToHash(name));

        StopCoroutine("SetAnimatorTrigger");
    }

    IEnumerator LerpRotation(Transform lerpObject, Quaternion targetRotation)
    {
        //Calculate distance between player and target
        float rotationDifference = Vector3.SqrMagnitude(lerpObject.eulerAngles - targetRotation.eulerAngles);

        //Lerp Player Position To match the animation
        //Lerp to Target 
        while (rotationDifference > 0.0001f)
        {
            //Calculate distance between player and target
            rotationDifference = Vector3.SqrMagnitude(lerpObject.eulerAngles - targetRotation.eulerAngles);


            yield return new WaitForEndOfFrame();

            //Lerp To Target
            lerpObject.localRotation = Quaternion.Lerp(lerpObject.localRotation, targetRotation, Time.deltaTime * 5);
        }



        StopCoroutine("LerpRotation");
    }
    #endregion




    #region Utility
    //will load the CurrentMinigameinfO Obj as per current minigame
    void LoadCurrentMiniGameInfo()
    {


        foreach (MiniGameBase miniGameBase in MiniGamesInThisLevel)
        {
            if (Globals.CurrentMiniGame == miniGameBase.miniGame)
            {
                currentMiniGameInfo = miniGameBase;
                return;
            }
        }

        //if (Globals.CurrentMiniGame == Globals.miniGame.boxJump || Globals.CurrentMiniGame == Globals.miniGame.spiderWall || Globals.CurrentMiniGame == Globals.miniGame.broadway)
        //    currentMiniGameInfo = MiniGamesInThisLevel[0];
        //else if(Globals.CurrentMiniGame == Globals.miniGame.stairClimb || Globals.CurrentMiniGame == Globals.miniGame.sideWall || Globals.CurrentMiniGame == Globals.miniGame.swingChase)
        //    currentMiniGameInfo = MiniGamesInThisLevel[1];
        //else if(Globals.CurrentMiniGame == Globals.miniGame.ramp)
        //    currentMiniGameInfo = MiniGamesInThisLevel[2];
    }

    //Gets the Current level's Unique name
    string GetCurrentLevelUniqueName()
    {
        switch (Globals.LoadedLevel)
        {
            case 0:
                return Globals.LevelUniqueName.Salmon.ToString();
            case 1:
                return Globals.LevelUniqueName.RockSwinger.ToString();
            case 2:
                return Globals.LevelUniqueName.BoxJumper.ToString();
            case 3:
                return Globals.LevelUniqueName.SpiderWall.ToString();
        }
        return null;
    }
    #endregion
}
