using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

public class BroadwayCreatorGameManager : MiniGameBase
{

    #region Parameters
    //Ref to Player's Animator component
    [SerializeField]
    Animator PlayerAnimator;

    //List of all triggers that can invoke The FastTap Controls behaviour
    [SerializeField]
    List<GameObject> AllTriggers = new List<GameObject>();

    //Represents the Fast Tap UI
    [SerializeField]
    FastTappingUI FastTapControlsUI;

    //Represents the BroadwayCreator's Animator component
    [SerializeField]
    Animator BroadwayCreatorAnimator;

    //Represents the BroadwayCreator's Animator component
    [SerializeField]
    Image FastTapProgressBar , RedVignetteImage;


    //If this time is past, then Game over
    [SerializeField]
    float deadlineTime = 4;
    #endregion


    #region Core
    public override void Initialize(bool flag)
    {
        //Enable all FastTap control Behaviour Triggers
        foreach (GameObject trigger in AllTriggers)
            trigger.SetActive(true);

        //Play Animations
        BroadwayCreatorAnimator.Play(Animator.StringToHash("broadwayCreator"), -1, 0);
        PlayerAnimator.Play(Animator.StringToHash(AnimatorTriggerName), -1, 0);
        //BroadwayCreatorAnimator.SetTrigger(Animator.StringToHash("brodwayCreator"));
        //PlayerAnimator.SetTrigger(Animator.StringToHash("miniGame_1"));

        base.Initialize(flag);
    }
    #endregion






    #region Callbacks
    public void OnBroadwayInteracted()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        BroadwayCreatorAnimator.SetFloat(Animator.StringToHash("speed"), 0.5f);
        PlayerAnimator.SetFloat(Animator.StringToHash("miniGameSpeed"), 0.5f);
        Timing.CallDelayed(1 , ()=> StartUIControlledBehaviour());
    }

    void StartUIControlledBehaviour()
    {


        FastTapControlsUI.enabled = true;
        //Show fastTap controls
        FastTapControlsUI.gameObject.SetActive(true);

        //Stop animation till the deadlineTime

        StartCoroutine(CheckIfTaskCompletedBeforeHand());

        StartCoroutine(StartDeadlineTimer());
    }

    void OnTaskComplete()
    {
        //RedVignetteImage.color = new Color(67, 255, 0, 30);
        StopAllCoroutines();

        //Set Red with 0 alpha
        RedVignetteImage.color = new Color(255, 0, 0, 0);
        //Lerp to 0 Visiblity
        // Timing.CallContinuously(deadlineTime, () => RedVignetteImage.color = Color.Lerp(RedVignetteImage.color, new Color(255, 0, 0, 0), Timing.DeltaTime * .05f));

        //RedVignetteImage.fillAmount = 0;
        //Singal GameManaer to Slow down Current minigameAnim Speed
        PlayerAnimator.SetFloat(Animator.StringToHash("miniGameSpeed"), 1f);
        BroadwayCreatorAnimator.SetFloat(Animator.StringToHash("speed"), 1f);
        
        GameObject.FindObjectOfType<GameManager>().ShowPerfectPanel();

        //Disbale fastTap controls Update
        FastTapControlsUI.enabled = false;
        FastTapProgressBar.fillAmount = 1;
        Timing.CallDelayed(1, () => FastTapControlsUI.gameObject.SetActive(false));
    }

    void OnTaskFailed()
    {
        //Stop playing Broadway animation
        BroadwayCreatorAnimator.SetFloat(Animator.StringToHash("speed"), 0);

        //Hide Fast Tap Controls
        FastTapControlsUI.enabled = false;


        //Singal Game over Behaviour
        GameObject.FindObjectOfType<GameManager>().DefeatedGameOver();
    }
    #endregion


    #region Coroutines
    IEnumerator StartDeadlineTimer()
    {
        //Set Red with 0 alpha
        RedVignetteImage.color = new Color(255, 0, 0, 0);

        StartCoroutine(LerpToColor(new Color(255, 0, 0, 255) , .003f));

        //Lerp to Red with Visiblity
        //Timing.CallContinuously(deadlineTime, () => RedVignetteImage.color = Color.Lerp(RedVignetteImage.color, new Color(255, 0, 0,255 ), Timing.DeltaTime * .003f));
        //Timing.
        //Wait till deadline Time
        yield return new WaitForSeconds(deadlineTime);

        //user Did not manage to completely fill the bar
        if(FastTapProgressBar.fillAmount < .9f)
        {
            OnTaskFailed();
        }
        else
        {
            OnTaskComplete();
        }
        StopAllCoroutines();
    }

    IEnumerator CheckIfTaskCompletedBeforeHand()
    {
        bool keepChecking = true;
        //task Complete
        while(keepChecking)
        {
            yield return new WaitForEndOfFrame();

            if (FastTapProgressBar.fillAmount > .95f)
            {
                OnTaskComplete();
                keepChecking = false;
            }
        }

        StopAllCoroutines();
    }

    IEnumerator LerpToColor(Color targetColor , float lerpMultiplier)
    {
        while (RedVignetteImage.color != targetColor)
        {
            yield return new WaitForEndOfFrame();

            //Lerp to Red with Visiblity
            RedVignetteImage.color = Color.Lerp(RedVignetteImage.color, targetColor, Timing.DeltaTime * lerpMultiplier);
        }
        StopCoroutine("LerpToColor");
    }

    #endregion
}
