using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

public class SpiderWallGameManager : MiniGameBase
{
    [SerializeField]
    Button ValidJump_Button , InvalidJump_Button;

    [SerializeField]
    Image TimerImageRef;

    //represents the Time within which the user has to click the Jump Button in order keep the Box Jumping progressing
    [SerializeField]
    float JumpTimerReductionMultipler;


    [SerializeField]
    bool KeepJumping = true;

    public override void Initialize(bool flag)
    {
        base.Initialize(flag);
    }

    public override void HideUI()
    {
        base.HideUI();
    }


    public void EnableControlsAsPerBehaviour()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), Globals.MiniGameAnimSpeedReductionMultiplier);

        //Show Controls
        ControlsUI.SetActive(true);

        //Start Button Behaviour as per Game
        StartCoroutine(TimeBasedJumpButtonBehaviour());
    }


    IEnumerator TimeBasedJumpButtonBehaviour()
    {
        KeepJumping = false;

        //Set Button Time based border fill ammount to Max
        TimerImageRef.fillAmount = 1;

        //Show timer Image
        TimerImageRef.gameObject.SetActive(true);

        //Enable Animator 
        ValidJump_Button.GetComponent<Animator>().enabled = true;

        //Show Valid Jump Button only , hide InvalidJumpButton
        ValidJump_Button.gameObject.SetActive(true);
        InvalidJump_Button.gameObject.SetActive(false);

        while (TimerImageRef.fillAmount > 0)
        {
            //Reduce Fill ammount at a given speed/Time
            TimerImageRef.fillAmount -= JumpTimerReductionMultipler * Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        //if Player has not pressed the Jump Button then He shall fall
        if (!KeepJumping)
        {
            //Make Player fall
            GameObject.FindObjectOfType<GameManager>().RagdollDeath();

            //Disable ValidJump Button Interaction
            ValidJump_Button.interactable = false;

            //Disable Valid Jump Anim
            ValidJump_Button.GetComponent<Animator>().enabled = false;
        }
        else
        {
            //Show InValid Jump Button only
            ValidJump_Button.gameObject.SetActive(false);
            InvalidJump_Button.gameObject.SetActive(true);

            //Hide timer Image
            TimerImageRef.gameObject.SetActive(false);
        }

        //Stop this Coroutine
        StopCoroutine(TimeBasedJumpButtonBehaviour());
    }



    public void OnValidJumpButtonPressed()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), 1);

        //Disable Animator So that DOtween can do it's thing
        ValidJump_Button.GetComponent<Animator>().enabled = false;

        //hide Border Image
        TimerImageRef.gameObject.SetActive(false);

        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowPerfectPanel();

        //Set Keep jumping to true
        KeepJumping = true;

        WrongMoveGraphic.SetActive(false);
        RightMoveGraphic.SetActive(true);
        Timing.CallDelayed(Globals.TimeToShowRightMoveVisual , () => RightMoveGraphic.SetActive(false));

        //Animate button
        //ValidJump_Button.GetComponent<CustomTween>().DoTween();
    }

    public void OnInValidJumpButtonPressed()
    {
        this.StopAllCoroutines();

        //Signa Ragdoll Death and Game over
        GameObject.FindObjectOfType<GameManager>().RagdollDeath();

        //Disable button Inteactable as we donot want the callback when game over
        InvalidJump_Button.interactable = false;

        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowTryAgainPanel();

        //Set Keep jumping to true
        KeepJumping = false;

        Globals.Vibrate();



        WrongMoveGraphic.SetActive(true);
        //Animate button
        //InvalidJump_Button.GetComponent<CustomTween>().DoTween();
    }


}
