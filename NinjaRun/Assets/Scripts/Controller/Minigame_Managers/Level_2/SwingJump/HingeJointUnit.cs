using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HingeJointUnit : MonoBehaviour
{
    //Represents the legit Position of the player if he grabs this Hinge
    public Transform PlayerPosition;

    [SerializeField]
    bool showPerfectUIOnGrab = false;


    void Start()
    {
        if (PlayerPosition == null)
            Debug.LogError("Player Position not Specified for " + gameObject.name);
        SetDefaults();
    }

    private void SetDefaults()
    {
        GetComponent<Rigidbody>().maxAngularVelocity = Globals.SwingRingMaxAngularVelocity;

        //Disable Collider
        GetComponent<BoxCollider>().enabled = true;
    }



    private void OnTriggerEnter(Collider other)
    {
        Behaviour(other);
    }

    void Behaviour(Collider other)
    {
        if (other.transform.root.tag != "Player")
            return;

        if (showPerfectUIOnGrab)
        {
            if (Globals.CurrentMiniGame == Globals.miniGame.swingJump)
                other.transform.root.GetComponent<CharacterMovement>().ShowPerfectUI();
            else if (Globals.CurrentMiniGame == Globals.miniGame.swingChase)
                GameObject.FindObjectOfType<GameManager>().ShowPerfectPanel();
        }
        // Debug.LogError("Should Grab Swing = " + other.gameObject);


        //Notify Swing Game Manager A new Swnig has been grabbed
        if (Globals.CurrentMiniGame == Globals.miniGame.swingJump)
            GameObject.FindObjectOfType<SwingGameManager>().OnNewSwingGrabbed(this);
        else if (Globals.CurrentMiniGame == Globals.miniGame.swingChase)
        {
            GameObject.FindObjectOfType<SwingChaseGameManager>().OnNewSwingGrabbed(this);
        }

        //Disable Collider
        GetComponent<BoxCollider>().enabled = false;
    }
}
