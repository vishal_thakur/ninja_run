using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MEC;

public class SwingGameManager : MonoBehaviour
{
    #region Parameters
    //Represents the Player's Transform Ref
    [SerializeField]
    Transform PlayerRef;

    //Represents the force with which the player shall Jump from the swing
    [SerializeField]
    float SwingJumpForce = 1.2f;

    //Represents the Player's RigidBody
    Rigidbody playerRigidBody;

    //Represents the HingeJoint's Rigidbody
    public Rigidbody CurrentlyGrabbedHingeRef;

    //List of all Hinge Joints
    public List<HingeJointUnit> AllHingeJoints = new List<HingeJointUnit>();

    //Represents the Direction of the force being applied to the Currently Grabbed Hinge
    Vector3 CurrentSwingDirection;

    //Represents the Force Applied to a direction while swinging
    [SerializeField]
    float forceValue;

    //Represents if the Hinge is in Swinging mode or not
    [SerializeField]
    bool swingModeActive = false;


    [SerializeField]
    bool isDraggingSlider = false;

    //[SerializeField]
    //bool isWaitingForSliderDraggedFalseCheck = false;

    //Represents the Slider UI that Swings the Ring when grabbed by the player
    [SerializeField]
    Slider SwingSliderUIRef;
    
    //Shows up at the right time to Jump
    [SerializeField]
    Image GreenVignetteImage;


    //Represents the Respawn point if gameover
    public Transform RespawnPoint;
    #endregion





    #region Core

    private void Start()
    {
        //Grab the player's Rigidbody Component
        playerRigidBody = PlayerRef.GetComponent<Rigidbody>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        if (RespawnPoint)
        {
            Gizmos.DrawCube(RespawnPoint.position, .5f * Vector3.one);
        }
    }

    private void Update()
    {
        //if(CurrentlyGrabbedHingeRef && CurrentlyGrabbedHingeRef.angularVelocity.x < 0)
        //    Debug.LogError(CurrentlyGrabbedHingeRef.angularVelocity.x);
        //else
        //    Debug.LogError("None");


        //If the slider is being Used but there is no dragging hence no swing
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Stationary)
            OnSliderDragged(false);
    }


    public void PerformSwing()
    {
        //Apply Torque to Hinge
        CurrentlyGrabbedHingeRef.AddTorque(-Vector3.right * (forceValue * SwingSliderUIRef.value), ForceMode.Impulse);

        //Save the Current Swing Direction
        CurrentSwingDirection = -Vector3.right * SwingSliderUIRef.value;
    }


    public void ToggleSwingBehaviour(bool flag)
    {
        //Set Swing Animation True/False based on the Flag
        PlayerRef.GetComponent<Animator>().SetBool("swing", flag);

        if (!swingModeActive && flag)
        {
            StartCoroutine(SwingPhysicsBehaviour());
        }


        if (!flag)
        {
            swingModeActive = false;
            StopCoroutine(SwingPhysicsBehaviour());
        }
    }

    public void ToggleHingeJointsCollider(bool flag)
    {
        foreach (HingeJointUnit hinge in AllHingeJoints)
            hinge.GetComponent<BoxCollider>().enabled = flag;
    }
    #endregion




    #region Callbacks

    public void OnSwingSliderMoved()
    {
        PerformSwing();
    }

    public void OnSwingSliderReleased()
    {
        //Signal Player Animator To Disable Swing
        //   PlayerRef.GetComponent<Animator>().SetBool("swing", false);
        //Timing.CallDelayed(.5f, ()=> SwingReleasedBehaviour());
        SwingReleasedBehaviour();
    }

    void SwingReleasedBehaviour()
    {

        //Signal Player Animator To Disable Swing
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingForward"), false);
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingBack"), false);
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingHangIdle"), false);


        //Show Jump from Swing animation
        // PlayerRef.GetComponent<Animator>().SetTrigger("jumpFromHang");
        //Show Jump from Swing animation
        PlayerRef.GetComponent<Animator>().SetTrigger("barOff");

        ToggleSwingBehaviour(false);

        //Hide Slider UI
        SwingSliderUIRef.gameObject.SetActive(false);

        //Deparent The Player from Current Swing
        PlayerRef.transform.SetParent(null);

        //Changes to Rigidbody Physics for Freefall/Jump
        playerRigidBody.isKinematic = false;
        playerRigidBody.useGravity = true;

        //Perform Jump
        playerRigidBody.AddForce((new Vector3(0.0f, CurrentSwingDirection.x, CurrentSwingDirection.x) * CurrentlyGrabbedHingeRef.angularVelocity.x * SwingJumpForce), ForceMode.Impulse);
    }

    //Called when the player makes a Good Jump and manages to approach a new Swing handle
    public void OnNewSwingGrabbed(HingeJointUnit newSwing)
    {
        //Show Hanging In Place animation
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingHangIdle"), true);
        PlayerRef.GetComponent<Animator>().SetTrigger("inPlaceHung");

        //Show Slider UI
        SwingSliderUIRef.gameObject.SetActive(true);

        //Make Player Child of this Swing
        PlayerRef.SetParent(newSwing.transform , true);

        PlayerRef.transform.eulerAngles = Vector3.zero;

        StartCoroutine(LerpPlayerRotationToHangingPose());
       // PlayerRef.transform.localPosition = new Vector3(0, -2.811f, -0.093f);
        
        //playerRigidBody.isKinematic = true;
        playerRigidBody.useGravity = false;
        playerRigidBody.isKinematic = true;

        //Signal Swing manager that a new Swing has been grabbed
        OnNewHingeGrabbed(newSwing);

        // SwingManagerRef.OnNewHingeGrabbed(newSwing);
        //Changes to Rigidbody so that It can Behave as IK again


    }



    public void OnSliderDragged(bool flag)
    {
        if (flag)
        {
            isDraggingSlider = true;
            ToggleSwingBehaviour(true);
        }
        else {
            if(isDraggingSlider)
                StartCoroutine(DelayedSliderDragRealeasedCheck());

            isDraggingSlider = false;
        }
    }

    public void OnNewHingeGrabbed(HingeJointUnit newSwing)
    {
        ToggleSwingBehaviour(false);

        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingBack"), false);
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingForward"), false);
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingHangIdle"), true);

        //Assign And Save the newly Grabbed Swing
        CurrentlyGrabbedHingeRef = newSwing.GetComponent<Rigidbody>();

        //Give an impulse to the newly grabbed Swing
        CurrentlyGrabbedHingeRef.AddTorque(-Vector3.right * 1.55f, ForceMode.Impulse);
    }
    #endregion



    #region Coroutines
    IEnumerator LerpPlayerRotationToHangingPose()
    {

        //Vector3 targetPosition = new Vector3(ClimbTarget.transform.position.x - .249f, ClimbTarget.transform.position.y - .118f, ClimbTarget.transform.position.z);
        Vector3 targetPosition = new Vector3(0, -2.811f, -0.093f);

        //Calculate Distance

        float positionDifference = Vector3.SqrMagnitude(PlayerRef.localPosition - targetPosition);
       // float rotationDifference = Vector3.SqrMagnitude(PlayerRef.eulerAngles - Vector3.zero);

        //Lerp to Target 
        while (positionDifference > 0.0001f )//&& rotationDifference > 0.0001f)
        {
            //Calculate position and Rotation Difference
            positionDifference = Vector3.SqrMagnitude(PlayerRef.localPosition - targetPosition);
         //   rotationDifference = Vector3.SqrMagnitude(PlayerRef.eulerAngles - Vector3.zero);

            yield return new WaitForEndOfFrame();

            //Lerp To Target
            PlayerRef.localPosition = Vector3.Lerp(PlayerRef.localPosition, targetPosition, Time.deltaTime * 15);
          //  PlayerRef.eulerAngles = Vector3.Lerp(PlayerRef.eulerAngles, Vector3.zero , Time.deltaTime * 2);
        }//Reached Target


        //Stop this Coroutine
        StopCoroutine(LerpPlayerRotationToHangingPose());
    }



    IEnumerator SwingPhysicsBehaviour()
    {
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingHangIdle"), false);
        swingModeActive = true;

        //Debug.LogError("Started Swinging");
        float swingDelay = 0.65f;
        float maxAngularVelocityAchieved = 0;
      //  float SwingAnimationStartSpeed = PlayerRef.GetComponent<Animator>().GetFloat("swingSpeed");
        // Vector3 lastSwingDirection = Vector3.left;

        //Swing to right
        // CurrentlyGrabbedHingeRef.AddTorque(Vector3.left * (forceValue * 10), ForceMode.Impulse);

        //LOOP, exit loop
        while (swingModeActive)
        {

            //Record Max Angular velocity Achieved in Front Direction
            maxAngularVelocityAchieved = CurrentlyGrabbedHingeRef.angularVelocity.x;


            //Swing to right
            //CurrentlyGrabbedHingeRef.AddTorque(Vector3.left * (forceValue) * Mathf.Abs(SwingSliderUIRef.value), ForceMode.Impulse);
            CurrentlyGrabbedHingeRef.AddTorque(Vector3.left * (forceValue) * Mathf.Abs(1), ForceMode.Impulse);
            PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingBack"), false);
            PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingForward"), true);
            //CurrentlyGrabbedHingeRef.AddTorque(Vector3.left * (forceValue), ForceMode.Impulse);
            CurrentSwingDirection = Vector3.left;

            //Show Green Vignette if Timing is right
            if (CurrentlyGrabbedHingeRef && (CurrentlyGrabbedHingeRef.angularVelocity.x <= 0f) && maxAngularVelocityAchieved <= -1.4f)
            {
                //Show Green Vignette as is the right time to jump
                GreenVignetteImage.color = new Color(GreenVignetteImage.color.r, GreenVignetteImage.color.g, GreenVignetteImage.color.b, .2f);
            }
            else//Donot show Green Vignette if timing is not right
                GreenVignetteImage.color = new Color(GreenVignetteImage.color.r, GreenVignetteImage.color.g, GreenVignetteImage.color.b, 0);

            yield return new WaitForSeconds(swingDelay);


            //Swing to Left
            //CurrentlyGrabbedHingeRef.AddTorque(Vector3.right * (forceValue) * Mathf.Abs(SwingSliderUIRef.value), ForceMode.Impulse);
            CurrentlyGrabbedHingeRef.AddTorque(Vector3.right * (forceValue) * Mathf.Abs(1), ForceMode.Impulse);
            PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingForward"), false);
            PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingBack"), true);
            //CurrentlyGrabbedHingeRef.AddTorque(Vector3.right * (forceValue), ForceMode.Impulse);

            CurrentSwingDirection = -Vector3.right;
            if (CurrentlyGrabbedHingeRef && (CurrentlyGrabbedHingeRef.angularVelocity.x <= 0f) && maxAngularVelocityAchieved <= -1.4f)
            {
                //Show Green Vignette as is the right time to jump
                GreenVignetteImage.color = new Color(GreenVignetteImage.color.r, GreenVignetteImage.color.g, GreenVignetteImage.color.b, .2f);
            }
            else//Donot show Green Vignette if timing is not right
                GreenVignetteImage.color = new Color(GreenVignetteImage.color.r, GreenVignetteImage.color.g, GreenVignetteImage.color.b, 0);
            //Donot show Green Vignette if timing is not right
            //GreenVignetteImage.color = new Color(GreenVignetteImage.color.r, GreenVignetteImage.color.g, GreenVignetteImage.color.b, 0);
            //Debug.LogError(swingDelay);
            if (swingDelay >= 0.54f)
            {
                swingDelay -= .01f;
               // SwingAnimationStartSpeed -= .001f;
            }

            //Set Swing Speed Float in the Animator
            //    PlayerRef.GetComponent<Animator>().SetFloat("swingSpeed", SwingAnimationStartSpeed);

            //Record Max Angular velocity Achieved in Front Direction
            maxAngularVelocityAchieved = CurrentlyGrabbedHingeRef.angularVelocity.x;
            yield return new WaitForSeconds(swingDelay);

        }
        PlayerRef.GetComponent<Animator>().SetBool(Animator.StringToHash("swingHangIdle"), true);
        //Stop this Coroutine
        StopCoroutine(SwingPhysicsBehaviour());
    }

    IEnumerator DelayedSliderDragRealeasedCheck()
    {
        yield return new WaitForSeconds(1);

        //If the Slidder is not being Dragged still after X seconds
        if (!isDraggingSlider)
        {
            Debug.LogError("stop Swinging");
            //Stop Swinging
            ToggleSwingBehaviour(false);
        }
        //else Do nothing and keep swinging

        //Stop this Coroutine
        StopCoroutine(DelayedSliderDragRealeasedCheck());
    }


    #endregion


}
