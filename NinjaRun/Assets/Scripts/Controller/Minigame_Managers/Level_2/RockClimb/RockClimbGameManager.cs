using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

public class RockClimbGameManager : MonoBehaviour
{

    #region Parameters
    //Represents the Player's Rigidbody References
    [SerializeField]
    Rigidbody PlayerRigidBodyRef;

    //Repreesnts the Rock that use Selected for the Player to jump towards
    [SerializeField]
    public GameObject initialClimbTarget, ClimbTarget , previousClimbTarget , gameStartTrigger;

    //Represents the Jump Speed
    [SerializeField]
    float JumpSpeed = 2 , MaxJumpDistance = 2;

    [SerializeField]
    bool AcceptInputs = false;

    //Ref to Player's Animator Component
    Animator PlayerAnimatorRef;

    //Represents the Current Jump's Direction
    [SerializeField]
    Vector3 JumpDirection;

    //Represents the Position where the player will lerp to , when this game ends
    [SerializeField]
    GameObject EndPosition;

    //point where the player respawns for this minigame
    [SerializeField]
    public Transform RespawnPoint;


    //Represents the list of all Rocks that can be grabbed by the player
    [SerializeField]
    List<Transform> AllRocks = new List<Transform>();
    #endregion


    #region Core

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        if (RespawnPoint)
        {
            Gizmos.DrawCube(RespawnPoint.position, .5f * Vector3.one);
        }
    }

    private void Start()
    {
        //Grab Player's Animator Ref 
        PlayerAnimatorRef = PlayerRigidBodyRef.GetComponent<Animator>();
        ResetDefaults();
    }

    public void ResetDefaults()
    {

        //Set current climb target 
        ClimbTarget = initialClimbTarget;

        gameStartTrigger.SetActive(true);
    }

    void Update()
    {
        if(AcceptInputs)
            checkTouch();
    }

    private void checkTouch ()
    {
#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {

            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit = new RaycastHit();
            raycastHit.distance = 1000;

            if (Physics.Raycast(raycast, out raycastHit))
            {
                //Disable Input For A while
                AcceptInputs = false;

                switch (raycastHit.collider.gameObject.layer)
                {
                    //Normal Climb Handle / Rock
                    case 3:
                       // Debug.LogError("Normal Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;

                    //Trap Climb Handle / Rock
                    case 6:
                        //Debug.LogError("Trap Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;

                    //Initial Climb Handle / Rock
                    case 7:
                        //Debug.LogError("Initial Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;

                    //Final  Climb Handle / Rock
                    case 8:
                        //Debug.LogError("End Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;
                }
            }
        }




        //}
#else
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit = new RaycastHit();
            raycastHit.distance = 1000;

            if (Physics.Raycast(raycast, out raycastHit))
            {
                //Disable Input For A while
                AcceptInputs = false;

                switch (raycastHit.collider.gameObject.layer)
                {
                    //Normal Climb Handle / Rock
                    case 3:
                       // Debug.LogError("Normal Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;

                    //Trap Climb Handle / Rock
                    case 6:
                        //Debug.LogError("Trap Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;

                    //Initial Climb Handle / Rock
                    case 7:
                        //Debug.LogError("Initial Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;

                    //Final  Climb Handle / Rock
                    case 8:
                        //Debug.LogError("End Climb");
                        JumpToClimbHandle(raycastHit.collider.gameObject);
                        break;
                }

            }
        }
#endif
    }



    //Jumps to the Next ClimbHandle in the List of handles as per the index
    public void JumpToClimbHandle(GameObject newclimbHandle)
    {
        MeshRenderer targetVisual;

        //Grab the Target Visual Mesh renderer component
        if (newclimbHandle == null)
        {
            targetVisual = ClimbTarget.transform.GetChild(1).GetComponent<MeshRenderer>();
        }
        else
        {
            targetVisual = newclimbHandle.transform.GetChild(1).GetComponent<MeshRenderer>();
            //Hide Pointer Visual
            newclimbHandle.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        }

        //Show Target Visual on the Newly selected ClimbHandle
        targetVisual.enabled = true;


        //Hide Target Visual for Previous ClimbHandle
        if (previousClimbTarget)
            previousClimbTarget.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;


        //Set The player Body To be kinematic
        PlayerRigidBodyRef.isKinematic = true;

        //Remove all contraints from Player Rigidbody
        PlayerRigidBodyRef.constraints = RigidbodyConstraints.None;

        //Stop all coroutines if any running
        StopAllCoroutines();//(LerpToTarget(false));

        //Jumping Between Rocks
        if (newclimbHandle != null)
        {
            //Save the Climb handle
            ClimbTarget = newclimbHandle;


            //Lerp Jump to Target
            Timing.CallDelayed(.2f, () => StartCoroutine(LerpToTarget(true)));
        }
        else//Initial Jump to climb Wall
        {
            //Jump to the Inspector Assigned Target
            Timing.CallDelayed(.5f, () => StartCoroutine(LerpToTarget(false)));

            //Play Idle to GrabRock animation
            PlayerAnimatorRef.SetTrigger("IdleToGrabRock");

        }
    }


    #endregion



    #region Coroutines

    IEnumerator ShowIndicatorsOnGrabableRocks(bool hideAllFlag)
    {
        float distance = 0;

        foreach (Transform rock in AllRocks)
        {
            if (hideAllFlag)
            {
                rock.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                rock.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                //Calculate distance between player and this rock
                distance = Vector3.SqrMagnitude(PlayerRigidBodyRef.position - new Vector3(rock.position.x, rock.position.y - 1.64f, rock.position.z - 0.415f));

                //If distance is less than max JumpDistance then show the indicator for this rock
                if (distance > .1f && distance <= MaxJumpDistance)
                {
                    rock.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                }
                else if (distance > MaxJumpDistance)//Hide the indicator for this rock
                {
                    rock.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                }
                else
                    rock.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            }
        }
        yield return new WaitForEndOfFrame();
    }


    IEnumerator LerpToTarget(bool playJumpAnimation)
    {
        //Get player Position
       // Vector3 playerPosition = PlayerRigidBodyRef.position;

        //Represents the Current Jump speed wrt the Target Rock
        float currentJumpSpeed;

        //Vector3 targetPosition = new Vector3(ClimbTarget.transform.position.x - .249f, ClimbTarget.transform.position.y - .118f, ClimbTarget.transform.position.z);
        Vector3 targetPosition = new Vector3(ClimbTarget.transform.position.x, ClimbTarget.transform.position.y - 1.64f, ClimbTarget.transform.position.z - 0.415f);

        //Calculate Distance

        float distance = Vector3.SqrMagnitude(PlayerRigidBodyRef.position - targetPosition);
        //Debug.ClearDeveloperConsole();
        //Calculate Jump Direction
        JumpDirection = PlayerRigidBodyRef.transform.position - ClimbTarget.transform.position;
        //Debug.LogError("Direction = " + JumpDirection.x);
        //Debug.LogError("Distance = " + distance);

        //JumpDirection = Vector3.Dot(PlayerRigidBodyRef.transform.right , JumpDirection) > 0 ? Vector3.left :Vector3.right;

        // Debug.LogError("Final Direction = " + JumpDirection);
        //Should Play Jump From Rock Animation or not
        if (playJumpAnimation)
        {
            //Play Jump Animation as per the Jump Direction
            if (JumpDirection.x > 0.5f)
            {
               // Debug.LogError("Left");
                Timing.CallDelayed(.2f, () => PlayerAnimatorRef.SetTrigger("RockJumpLeft"));
            }
            else if (JumpDirection.x < -0.5f && JumpDirection.x < 0)
            {
               // Debug.LogError("right");
                Timing.CallDelayed(.2f, () => PlayerAnimatorRef.SetTrigger("RockJumpRight"));
            }
            else if(JumpDirection.x > -0.5f && JumpDirection.x < 0.5f)
            {
             //   Debug.LogError("Up/Down");
                PlayerAnimatorRef.SetTrigger("RockJumpUp");
            }
            yield return new WaitForSeconds(.5f);
        }



        //If the Target Rock is too far then the player will make a jump but Fall
        if (distance > MaxJumpDistance)
        {
            //Let player Jump and cover some distane , and then fall
            Timing.CallDelayed(.3f, () => OnJumpFailed());

            //Reduce Jump Speed
            currentJumpSpeed = JumpSpeed / 4f;
        }
        else
            currentJumpSpeed = JumpSpeed;

        //Lerp to Target 
        while (distance > 0.0001f)
        {
            //Calculate distance between player and target
            distance = Vector3.SqrMagnitude(PlayerRigidBodyRef.position - targetPosition);

            //Get player Position
           // playerPosition = PlayerRigidBodyRef.position;

            yield return new WaitForEndOfFrame();

            //Lerp To Target
            PlayerRigidBodyRef.position = Vector3.Lerp (PlayerRigidBodyRef.position, targetPosition, Time.deltaTime * currentJumpSpeed);
        }//Reached Target

        StartCoroutine(ShowIndicatorsOnGrabableRocks(false));

        //Save Previous Climb Target
        previousClimbTarget = ClimbTarget;

        //Enable input in certain cases only Normal Rock | Initial Rock
        if (ClimbTarget.layer == 3 || ClimbTarget.layer == 7) { 
            AcceptInputs = true;
        }

        //If the Player just climbed the Final Rock end the game
        if (ClimbTarget.layer == 8)
        {
            StartCoroutine(EndGame());

            //Hide Target Visual for this Trap Handle
            ClimbTarget.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
        }

        //If Player Just Climbed a Trap Rock then he will fall
        if(ClimbTarget.layer == 6)
        {
            //Hide Target Visual for this Trap Handle
            ClimbTarget.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;

            Timing.CallDelayed(.3f, () => OnJumpFailed());

            //Make the Grabbed Rock fall aswell
            Timing.CallDelayed(.3f, () => ClimbTarget.GetComponent<Rigidbody>().useGravity = true);
            Timing.CallDelayed(.3f, () => ClimbTarget.GetComponent<Rigidbody>().isKinematic = false);
        }

    }

    #endregion




    #region Behaviours

    //End Game Behaviour
    IEnumerator EndGame()
    {
        //Hide Ondicators If any active
        StartCoroutine(ShowIndicatorsOnGrabableRocks(true));
        PlayerRigidBodyRef.GetComponent<CharacterMovement>().ShowPerfectUI();
        GameObject.FindObjectOfType<CameraMovement>().ShowDefaultView();

        yield return new WaitForSeconds(.5f);
        //Play climbing Ramp Animation
        PlayerAnimatorRef.SetTrigger("RockGameFinish");
        //yield return new WaitForEndOfFrame();
     //   Debug.LogError("End Game Climb up the wall");


        Vector3 targetPosition = EndPosition.transform.position;
        float distance = Vector3.SqrMagnitude(PlayerRigidBodyRef.position - targetPosition);


        //Lerp Player Position To match the animation
        //Lerp to Target 
        while (distance > 0.0001f)
        {
          //  Debug.LogError(distance.ToString());
            //Calculate distance between player and target
            distance = Vector3.SqrMagnitude(PlayerRigidBodyRef.position - targetPosition);

            //Get player Position
          // playerPosition = PlayerRigidBodyRef.position;

            yield return new WaitForEndOfFrame();

            //Lerp To Target
            PlayerRigidBodyRef.position = Vector3.Lerp(PlayerRigidBodyRef.position, targetPosition, Time.deltaTime * 2.7f);
        }

        //Debug.LogError("Start Running Further");


        //Play climbing Ramp Animation

        PlayerRigidBodyRef.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX;
        PlayerRigidBodyRef.GetComponent<CharacterMovement>().Run();
        PlayerRigidBodyRef.isKinematic = false;
       // StopAllCoroutines();
        //Set the needful parameters

    }


    //Jump Failed Behaviour
    public void OnJumpFailed()
    {

        this.StopAllCoroutines();
        PlayerRigidBodyRef.GetComponent<CharacterMovement>().ShowMissPanel();
        //Hide Ondicators If any active
        StartCoroutine(ShowIndicatorsOnGrabableRocks(true));

        PlayerRigidBodyRef.constraints = RigidbodyConstraints.FreezePositionX;
        PlayerRigidBodyRef.constraints = RigidbodyConstraints.FreezeRotation;
        PlayerRigidBodyRef.isKinematic = false;
        PlayerRigidBodyRef.useGravity = true;

        //Gameover
        PlayerAnimatorRef.SetTrigger("death");

    }
    #endregion
}
