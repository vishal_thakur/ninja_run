using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbHandleUnit : MonoBehaviour
{
    Vector3 orignalPosition;

    private void Start()
    {
        orignalPosition = transform.localPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
       // Debug.LogError(other.gameObject.name);
        if (other.tag.Equals("Player") || other.transform.root.tag.Equals("Player"))
        {
            //GameObject.FindObjectOfType<RockClimbGameManager>().OnHandleGrabbed(this);
           // this.GetComponent<BoxCollider>().enabled = false;
        }
        //Reset Climb Handle ot orignal position
        if(other.gameObject.name == "Plane")
        {
            MEC.Timing.CallDelayed(1.3f, () => Reset());
        }
    }

    void Reset()
    {

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = true;
        transform.localPosition = orignalPosition;
    }


}
