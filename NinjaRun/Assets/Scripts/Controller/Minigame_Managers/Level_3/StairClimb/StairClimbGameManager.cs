using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

public class StairClimbGameManager : MiniGameBase
{
    #region Parameters
    [SerializeField]
    Button ValidLeftButton, ValidRightButton, InValidLeftButton, InValidRightButton;

    [SerializeField]
    Image LeftButtonTimerImage, RightButtonTimerImage;

    //represents the Time within which the user has to click the Jump Button in order keep the Box Jumping progressing
    [SerializeField]
    float JumpTimerReductionMultipler;

    //If true player wont fall, otherwise fall and gameover
    [SerializeField]
    bool KeepJumping = true;
    #endregion



    public override void Initialize(bool flag)
    {
        base.Initialize(flag);
    }

    public override void HideUI()
    {
        base.HideUI();
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
    }



    public void EnableControlsAsPerBehaviour()
    {
//        Debug.LogError("Starting behaviour" + Time.time);
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), Globals.MiniGameAnimSpeedReductionMultiplier);

        //Show Controls
        ControlsUI.SetActive(true);

        //Start Time based behaviour for Right Button First and then Left Button Later
        StartCoroutine(TimeBasedRightButtonBehaviour());

    }


    IEnumerator TimeBasedRightButtonBehaviour()
    {
        KeepJumping = false;

        //Set Button Time based border fill ammount to Max
        RightButtonTimerImage.fillAmount = 1;

        //Show Right button timer Image
        RightButtonTimerImage.gameObject.SetActive(true);

        //Enable animator
        ValidRightButton.GetComponent<Animator>().enabled = true;

        //Show ValidRight button
        ValidRightButton.gameObject.SetActive(true);

        //Disable Right button Interaction
        ValidRightButton.interactable = true;

        //Hide InvalidRight button
        InValidRightButton.gameObject.SetActive(false);


        //Reduce fill for right timer image
        while (RightButtonTimerImage.fillAmount > 0)
        {
            RightButtonTimerImage.fillAmount -= JumpTimerReductionMultipler * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //if Player has not pressed the Jump Button then He shall fall
        if (!KeepJumping)
        {
            Failed();
        }
        else
        {
            //Hide Valid right Button
            ValidRightButton.gameObject.SetActive(false);

            //Show Invalid right button
            InValidRightButton.gameObject.SetActive(true);
            //Start ValidLeft Button time based Behaviour now
            StartCoroutine(TimeBasedLeftButtonBehaviour());
        }
        //Stop this Coroutine
        StopCoroutine(TimeBasedRightButtonBehaviour());
    }

    IEnumerator TimeBasedLeftButtonBehaviour()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), Globals.MiniGameAnimSpeedReductionMultiplier);

        KeepJumping = false;

        //Set Button Time based border fill ammount to Max
        LeftButtonTimerImage.fillAmount = 1;

        //Show Right button timer Image
        LeftButtonTimerImage.gameObject.SetActive(true);

        //Enable animator
        ValidLeftButton.GetComponent<Animator>().enabled = true;

        //Show ValidRight button
        ValidLeftButton.gameObject.SetActive(true);

        //Enable Right button Interaction
        ValidLeftButton.interactable = true;

        //Hide InvalidRight button
        InValidLeftButton.gameObject.SetActive(false);


        //Reduce fill for right timer image
        while (LeftButtonTimerImage.fillAmount > 0)
        {
            LeftButtonTimerImage.fillAmount -= JumpTimerReductionMultipler * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //if Player has not pressed the Jump Button then He shall fall
        if (!KeepJumping)
        {
            Failed();
        }
        else
        {

            //Hide Valid Button
            ValidLeftButton.gameObject.SetActive(false);

            //Show Invalid button
            InValidLeftButton.gameObject.SetActive(true);
        }




        //Stop this Coroutine
        StopCoroutine(TimeBasedLeftButtonBehaviour());
    }



    public void OnValidRightButtonPressed()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), 1f);

        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowPerfectPanel();

        //Set Keep jumping to true
        KeepJumping = true;

        //Disable animator and doTween
        ValidRightButton.GetComponent<Animator>().enabled = false;

        RightButtonTimerImage.gameObject.SetActive(false);

        WrongMoveGraphic.SetActive(false);
        RightMoveGraphic.SetActive(true);
        Timing.CallDelayed(Globals.TimeToShowRightMoveVisual , () => RightMoveGraphic.SetActive(false));

        //ValidRightButton.GetComponent<CustomTween>().DoTween();

        ValidRightButton.interactable = false;
    }

    public void OnValidLeftButtonPressed()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), 1f);

        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowPerfectPanel();

        //Set Keep jumping to true
        KeepJumping = true;

        LeftButtonTimerImage.gameObject.SetActive(false);

        //Disable animator and doTween
        ValidLeftButton.GetComponent<Animator>().enabled = false;

        WrongMoveGraphic.SetActive(false);
        RightMoveGraphic.SetActive(true);
        Timing.CallDelayed(Globals.TimeToShowRightMoveVisual , () => RightMoveGraphic.SetActive(false));

        //ValidLeftButton.GetComponent<CustomTween>().DoTween();

        ValidLeftButton.interactable = false;
    }

    public void OnInValidRightButtonPressed()
    {
        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowTryAgainPanel();

        //Set Keep jumping to true
        KeepJumping = false;

        Failed();


        WrongMoveGraphic.SetActive(true);
        //InValidRightButton.GetComponent<CustomTween>().DoTween();

    }

    public void OnInValidLeftButtonPressed()
    {
        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowTryAgainPanel();

        //Set Keep jumping to true
        KeepJumping = false;

        Failed();


        WrongMoveGraphic.SetActive(true);
        //InValidLeftButton.GetComponent<CustomTween>().DoTween();
    }

    void Failed()
    {
        //Make player Fall
        GameObject.FindObjectOfType<GameManager>().RagdollDeath();

        //Disable All button Interactions
        ValidLeftButton.interactable = InValidLeftButton.interactable = ValidRightButton.interactable = InValidRightButton.interactable = false;
    }
}
