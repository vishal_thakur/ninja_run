using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxUnit : MonoBehaviour
{


    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            //this.GetComponent<BoxCollider>().enabled = false;
            GameObject.FindObjectOfType<BoxJumpGameManager>().EnableControlsAsPerBehaviour();

            //Disable this Collider
            this.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
