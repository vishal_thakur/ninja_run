using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

public class BoxJumpGameManager : MiniGameBase
{
    [SerializeField]
    Button ValidJump_Button , InvalidJump_Button;

    [SerializeField]
    Image TimerImageRef;

    //represents the Time within which the user has to click the Jump Button in order keep the Box Jumping progressing
    [SerializeField]
    float JumpTimerReductionMultipler;


    [SerializeField]
    bool KeepJumping = true;

    public override void Initialize(bool flag)
    {
        base.Initialize(flag);
    }

    public override void HideUI()
    {
        base.HideUI();
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
    }


    public void EnableControlsAsPerBehaviour()
    {
        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed") ,Globals.MiniGameAnimSpeedReductionMultiplier);

        //Show Controls
        ControlsUI.SetActive(true);

        //Start Button Behaviour as per Game
        StartCoroutine(TimeBasedJumpButtonBehaviour());
    }


    IEnumerator TimeBasedJumpButtonBehaviour()
    {
        KeepJumping = false;

        //Set Button Time based border fill ammount to Max
        TimerImageRef.fillAmount = 1;

        //Show timer Image
        TimerImageRef.gameObject.SetActive(true);

        //Enable Animator 
        ValidJump_Button.GetComponent<Animator>().enabled = true;

        //Show Valid Jump Button only
        ValidJump_Button.gameObject.SetActive(true);
        InvalidJump_Button.gameObject.SetActive(false);

        while (TimerImageRef.fillAmount > 0)
        {
            TimerImageRef.fillAmount -= JumpTimerReductionMultipler * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //if Player has not pressed the Jump Button then He shall fall
        if (!KeepJumping)
        {
            Failed();
        }
        else
        {
            //Show InValid Jump Button only
            ValidJump_Button.gameObject.SetActive(false);
            InvalidJump_Button.gameObject.SetActive(true);

            //Hide timer Image
            TimerImageRef.gameObject.SetActive(false);
        }

        //Stop this Coroutine
        StopCoroutine(TimeBasedJumpButtonBehaviour());
    }



    public void OnValidJumpButtonPressed()
    {

        //Singal GameManaer to Slow down Current minigameAnim Speed
        GameObject.FindObjectOfType<GameManager>().playerAnimator.SetFloat(Animator.StringToHash("miniGameAnimSpeed"), 1);

        //Disable Animator So that DOtween can do it's thing
        ValidJump_Button.GetComponent<Animator>().enabled = false;

        //hide Border Image
        TimerImageRef.gameObject.SetActive(false);

        WrongMoveGraphic.SetActive(false);
        RightMoveGraphic.SetActive(true);
        Timing.CallDelayed(Globals.TimeToShowRightMoveVisual , ()=> RightMoveGraphic.SetActive(false));
        //Show Green Vignette for A second
        //ValidJump_Button.GetComponent<CustomTween>().DoTween();

        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowPerfectPanel();

        //Set Keep jumping to true
        KeepJumping = true;

        //this.StopAllCoroutines();
    }

    public void OnInValidJumpButtonPressed()
    {
        this.StopAllCoroutines();

        //Show Perfect Panel
        GameObject.FindObjectOfType<GameManager>().ShowTryAgainPanel();

        //Set Keep jumping to true
        KeepJumping = false;

        Globals.Vibrate();

        Failed();

        //Show Red Vignette
        //Do Tween
        WrongMoveGraphic.SetActive(true);
        //   InvalidJump_Button.GetComponent<CustomTween>().DoTween();

    }

    void Failed()
    {
        //Make player Fall
        GameObject.FindObjectOfType<GameManager>().RagdollDeath();

        //Disable All button Interactions
        ValidJump_Button.interactable = InvalidJump_Button.interactable = false;


        //Show Right button timer Image
        TimerImageRef.gameObject.SetActive(false);
    }


}
