using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

public class RampClimbGameManager : MiniGameBase
{

    //represents the Filling bar Image
    [SerializeField]
    Image FillingBarImage;

    [SerializeField]
    float barFillPercentage = 0;

    //Represents the player Animator Ref
    [SerializeField]
    Animator PlayerAnim;

    //the value Above which the playe can climb the ramp, if the bar percentage is less than this , then Player Shall fall
    [SerializeField]
    float FallThreshhold = 0;

    [SerializeField]
    bool CheckIfPlayerLeftTapping = true;

    public override void Initialize(bool flag)
    {
        base.Initialize(flag);
    }

    public override void HideUI()
    {
        base.HideUI();
    }

    public override void Finish()
    {
        base.Finish();
    }
    

    private void Update()
    {
        ValidatePlayerMovement();
    }



    void ValidatePlayerMovement()
    {
        //Calculate bar Fill percentage
        barFillPercentage = FillingBarImage.fillAmount;


        UpdateFallThreshHold();


        //PlayerAnim.GetCurrentAnimatorClipInfo(0).;

        if (barFillPercentage <= 0.5f)
        {
            PlayerAnim.SetFloat("rampClimbSpeed", 0.5f);
        }
        else if (barFillPercentage > .8f)
        {
            PlayerAnim.SetFloat("rampClimbSpeed", 1
                );
        }
        else
        {
            PlayerAnim.SetFloat("rampClimbSpeed", barFillPercentage);
        }

        if (CheckIfPlayerLeftTapping)
        {

            if (barFillPercentage > 0 && barFillPercentage < FallThreshhold)
            {
                StartCoroutine(CheckIfPlayerHasLeftFastTapping());
                CheckIfPlayerLeftTapping = false;
            }
        }
    }

    void UpdateFallThreshHold()
    {

        if (barFillPercentage > FallThreshhold)
        {
            FallThreshhold = barFillPercentage;//Mathf.Clamp(barFillPercentage - .05f , 0 , 1);
        }
       // Debug.LogError("value : " + barFillPercentage + "|||| threshhold : " + FallThreshhold);
    }

    IEnumerator CheckIfPlayerHasLeftFastTapping()
    {
       // Debug.LogError("No Tapping , will wait till 2 seconds if player starts tapping");
        //Wait for 2 seconds and Check if Player can continue climbing
        yield return new WaitForSeconds(.5f);

        //X seconds have past , still Player Has not started Fast Tapping, Make him fall
        if (barFillPercentage + .05f < FallThreshhold)
        {
            //Make Player fall
            GameObject.FindObjectOfType<GameManager>().RagdollDeath();
            HideUI();
            this.gameObject.SetActive(false);
        }
        else//Keep Going
            CheckIfPlayerLeftTapping = true;

        StopCoroutine(CheckIfPlayerHasLeftFastTapping());
    }
}
