using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorTest : MonoBehaviour
{
    [SerializeField]
    Animator anim;

    [SerializeField]
    Vector3 LastPosition;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    [SerializeField]
    float currentTime = 0;


    // Update is called once per frame
    void FixedUpdate()
    {

        //public float GetCurrentAnimatorTime(Animator targetAnim, int layer = 0)
        //{
        //  return currentTime;
        //}

        if (Input.GetKeyUp(KeyCode.Space))
        {
            //Set Last Position
            transform.position = LastPosition;

            //Play from the time that it was stopped
            anim.Play("Base Layer.MiniGame_2", 0, currentTime);
        }


        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            anim.StopPlayback();
            //Save Time the animation is stopped
            AnimatorStateInfo animState = anim.GetCurrentAnimatorStateInfo(0);
            currentTime = animState.normalizedTime % 1;

            LastPosition = transform.position;
            anim.SetTrigger("slipRight");
        }
    }
}
