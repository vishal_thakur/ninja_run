//using Pathfinding;
//using ProjectUtility.Utilities.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLevelSlider : MonoBehaviour
{
    [SerializeField]
    Image progressBar;
    float totalDistance = 0;
    float distanceRemaining = 0;
    //public Slider LevelSlider;
    public Transform initialPos;
    public Transform playerPos;
    public Transform finalPos;
    //public GameObject arrowObj, crownObj;
    //[SerializeField] private RectTransform playerIndicator;
    //public List<RectTransform> aIIndicators;
    //public List<Transform> aiPositions;
    //public List<PlayerElement> aiElements;
    //public PlayerElement playerElement;
    bool runScript;
    //float aiValue;


    private void Start()
    {
        totalDistance = Vector3.Distance(initialPos.position, finalPos.position);
    }
    private void Update()
    {
        distanceRemaining = totalDistance - Vector3.Distance(playerPos.position, finalPos.position);
        //Debug.LogError("TotalDist = " + totalDistance);
        //Debug.LogError("distanceRemaining = " + distanceRemaining); 
        //Debug.LogError("Progress = " + (distanceRemaining / totalDistance)); 

        //Debug.LogError("Current Value = " + (distanceRemaining / totalDistance));
        progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount , (distanceRemaining / totalDistance) , Time.deltaTime * 2f);
    }

}
