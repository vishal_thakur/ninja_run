using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;


public class FastTappingUI : MonoBehaviour
{
    [SerializeField]
    GameObject InstructionsHolderUI;

    public float decreaseDelta;
    public float increaseDelta;

    public Image progressBar;
    public float tapDelayEnd;

    public bool TapDelayEnded;

    public GameObject touchFeedback;
    public Transform touchFeedbackParent;

    // Start is called before the first frame update
    void OnEnable()
    {
        //Set Progress bar fill amount to 0
        progressBar.fillAmount = .3f;

        //Show instructions
        InstructionsHolderUI.SetActive(true);
    }


    // Update is called once per frame
    void Update()
    {
        float decreaseVal = (TapDelayEnded ? decreaseDelta * 3 : decreaseDelta);
        progressBar.fillAmount = Mathf.Clamp(progressBar.fillAmount - decreaseVal * Time.deltaTime, 0, 1);
    }


    public void OnTap()
    {
        if (!this.enabled)
            return;

        StopAllCoroutines();
        TapDelayEnded = false;
        StartCoroutine(CheckforTapDelay());
        GameObject obj = (GameObject)Instantiate(touchFeedback, touchFeedbackParent);
        obj.transform.position = Input.mousePosition;
        Timing.CallDelayed(.5f , ()=> Destroy(obj));
        progressBar.fillAmount += increaseDelta;
        InstructionsHolderUI.SetActive(false);

    }

    IEnumerator CheckforTapDelay()
    {
        yield return new WaitForSeconds(tapDelayEnd);
        TapDelayEnded = true;
    }
}