using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreenManager : MonoBehaviour
{

    //Will load the fill in X seconds
    [SerializeField]
    float lerpSpeed = .1f;

    [SerializeField]
    Image FillImage;

    [SerializeField]
    GameObject BeginUIPanel;


    // Start is called before the first frame update
    void Start()
    {
        if (Globals.hasShownSplash)
        {
            gameObject.SetActive(false);
            BeginUIPanel.SetActive(true);
        }
        else
        {
            BeginUIPanel.SetActive(false);

            StartCoroutine(FillLoadingBar());
        }
        
    }


    IEnumerator FillLoadingBar()
    {
        while (!Mathf.Approximately(1 , FillImage.fillAmount))
        {
            FillImage.fillAmount += Time.deltaTime * lerpSpeed;
            yield return new WaitForEndOfFrame();
        }
        FillImage.fillAmount = 1;
        StopCoroutine(FillLoadingBar());
        this.gameObject.SetActive(false);
        BeginUIPanel.SetActive(true);
        Globals.hasShownSplash = true;
    }
}
